var dataphongkham = [
  {
    "STT": 1,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tSố 258 Cao Thắng, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9603082,
    "Latitude": 107.0932535
  },
  {
    "STT": 2,
    "Name": "\tPhòng khám Nội Hòa An\t",
    "address": "\tSố nhà 276, tổ 19, khu 2, Phường Cao Thắng, thành phố  Hạ Long. Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 3,
    "Name": "\tNha Khoa Hòa An\t",
    "address": "\tSố nhà 276, tổ 19, khu 2, phường Cao Thắng, thành phố  Hạ Long, tỉnh Quảng Ninh.\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 4,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt\t",
    "address": "\tSố nhà 116, Kênh Liêm, phường Cao Thắng,  thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9585441,
    "Latitude": 107.0913845
  },
  {
    "STT": 5,
    "Name": "\tPhòng Chẩn Trị Yhdt Thái Thịnh Đường\t",
    "address": "\tSố nhà 313, tổ 30, khu 3, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.966594,
    "Latitude": 107.096763
  },
  {
    "STT": 6,
    "Name": "\tNha Khoa Quốc Tế\t",
    "address": "\tSố nhà 121 Kênh Liêm, phường Cao Thắng, thành phố  Hạ Long, tỉnh Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9585341,
    "Latitude": 107.0913577
  },
  {
    "STT": 7,
    "Name": "\tNha Khoa Nụ Cười Việt\t",
    "address": "\tSố nhà 211, tổ 14, khu 2A, phường Cao Thắng , thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9685205,
    "Latitude": 107.1604125
  },
  {
    "STT": 8,
    "Name": "\tPhòng khám Kiên Cường\t",
    "address": "\tSố nhà 212, tổ 13, khu 2A, P. Cao Thắng, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9711977,
    "Latitude": 107.0448069
  },
  {
    "STT": 9,
    "Name": "\tPhòng khám Bác Sỹ Tuấn Cao Xanh\t",
    "address": "\tSố nhà 103, tổ 20C, khu 2, phường Cao Xanh, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9722852,
    "Latitude": 107.0834734
  },
  {
    "STT": 10,
    "Name": "\tPhòng khám  Hoàng Tùng\t",
    "address": "\tSố 6, tổ 4, khu I, Phường Cao Xanh, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9722852,
    "Latitude": 107.0834734
  },
  {
    "STT": 11,
    "Name": "\tPhòng khám Bác Sỹ Cao Minh Phương\t",
    "address": "\tTổ 67D, khu 6, phường Cao Xanh, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.97945,
    "Latitude": 107.0886
  },
  {
    "STT": 12,
    "Name": "\tPhòng khám Tai Mũi Họng\t",
    "address": "\tSố nhà 126A, tổ 1, khu 1,  phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 13,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt 103\t",
    "address": "\tSố nhà 9, tổ 1, khu 1, phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 14,
    "Name": "\tPhòng khám Nội Khoa Phương Thuý\t",
    "address": "\tTổ 1, khu1, Đại Yên, Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9828115,
    "Latitude": 106.9240554
  },
  {
    "STT": 15,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Hậu Sinh Đường\t",
    "address": "\tTổ 1, khu 3, phường Giếng Đáy, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9724879,
    "Latitude": 107.0111315
  },
  {
    "STT": 16,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tSố 140, tổ 4, khu 3, Giếng Đáy, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9769346,
    "Latitude": 107.022336
  },
  {
    "STT": 17,
    "Name": "\tDịch Vụ Y Tế Vũ Thị Hòa\t",
    "address": "\tKi ốt chợ Giếng Đáy, P. Giếng Đáy, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9775891,
    "Latitude": 107.0084098
  },
  {
    "STT": 18,
    "Name": "\tPhòng khám Nha Khoa Trung Đức\t",
    "address": "\tSố 91 Đường Hạ Long, P. Giếng Đáy, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9676684,
    "Latitude": 106.994559
  },
  {
    "STT": 19,
    "Name": "\tPhòng khám  Nội Khoa\t",
    "address": "\tTổ 7, khu I, P. Giếng Đáy, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9771229,
    "Latitude": 107.0179173
  },
  {
    "STT": 20,
    "Name": "\tPhòng khám Chữa Bệnh Bác Sỹ Đức Quyết\t",
    "address": "\tTổ 9, khu 2, Phường Giếng Đáy, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9821042,
    "Latitude": 107.0174661
  },
  {
    "STT": 21,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Thái Y Đường\t",
    "address": "\tSố nhà 29, tổ 12, khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 22,
    "Name": "\tDịch Vụ Y Tế Phan Thị Hường\t",
    "address": "\tTổ 7, khu 2, phường Giếng Đáy, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9815961,
    "Latitude": 107.0210624
  },
  {
    "STT": 23,
    "Name": "\tPhòng khám Chữa Bệnh Chuyên Khoa Phụ Sản\t",
    "address": "\tSố nhà 235, tổ 8, khu 6, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 24,
    "Name": "\tPhòng khám Siêu Âm Vì Sức Khỏe Cộng Đồng\t",
    "address": "\tSố nhà 43 Tổ 6, khu 6, phường Giếng Đáy, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 25,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tSố nhà 38 , tổ 1 khu 1, phường Giếng Đáy, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 26,
    "Name": "\tDịch Vụ Y Tế Bùi Văn Đàm\t",
    "address": "\tSố nhà 146, tổ 4, khu 3, phường Giếng Đáy, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9815272,
    "Latitude": 107.0214647
  },
  {
    "STT": 27,
    "Name": "\tNha Khoa Việt Úc Cơ Sở Ii\t",
    "address": "\tSố nhà 89 đường Giếng Đáy, P. Giếng Đáy , thành phố, Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9771229,
    "Latitude": 107.0179173
  },
  {
    "STT": 28,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Tư Nhân\t",
    "address": "\tSố 415, tổ 48, khu 5, P. Hà Khẩu, Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9736092,
    "Latitude": 106.9860384
  },
  {
    "STT": 29,
    "Name": "\tPhòng khám Nội Khoa\t",
    "address": "\tSố 160 đường An Tiêm, phường Hà Khẩu, thành phố  Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9860989,
    "Latitude": 106.9962519
  },
  {
    "STT": 30,
    "Name": "\tPhòng khám Đông Y Trần Minh Tuyến\t",
    "address": "\tTổ 38, khu 4, phường Hà Khẩu,thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.97857,
    "Latitude": 107.006052
  },
  {
    "STT": 31,
    "Name": "\tPhòng khám Răng Hàm Mặt Nha Khoa Minh Đức - Mai Anh\t",
    "address": "\tSố 59, tổ 68, khu 7, P. Hà Khẩu, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9736092,
    "Latitude": 106.9860384
  },
  {
    "STT": 32,
    "Name": "\tDịch Vụ Làm Răng Giả\t",
    "address": "\tTổ 3, khu 4, phường Hà Lầm, thành phố  Hạ Long, QN\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9649111,
    "Latitude": 107.1071774
  },
  {
    "STT": 33,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền\t",
    "address": "\tSố 501 phố Đoàn Kết, phường Hà Lầm, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.970539,
    "Latitude": 107.1144837
  },
  {
    "STT": 34,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\tTổ 4 khu 4, P. Hà Lầm, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.951072,
    "Latitude": 107.134049
  },
  {
    "STT": 35,
    "Name": "\tPhòng khám Nha Khoa Việt\t",
    "address": "\tSố 566 - tổ 1 khu 6, P. Hà Lầm, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 36,
    "Name": "\tSiêu Âm An Sinh\t",
    "address": "\tSố nhà 06, tổ 1, khu 6, phường Hà Lầm, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 37,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tSố 62,Tổ 48B, Khu 4B, Phường Hà Phong, thành phố Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.976522,
    "Latitude": 107.1543601
  },
  {
    "STT": 38,
    "Name": "\tPhòng khám Thu Hoài  Chuyên Khoa Da Liễu\t",
    "address": "\tTổ 38A, Khu 4, Phường Hà Phong, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.976522,
    "Latitude": 107.1543601
  },
  {
    "STT": 39,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tTổ 8B, Khu I, phường Hà Phong, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.976522,
    "Latitude": 107.1543601
  },
  {
    "STT": 40,
    "Name": "\tPhòng khám Nội Ngoài Giờ\t",
    "address": "\tTổ 48B, Khu 4B, phường Hà Phong, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.976522,
    "Latitude": 107.1543601
  },
  {
    "STT": 41,
    "Name": "\tDịch Vụ Y Tế - Công Ty Tnhh Cssk Hoàng Anh\t",
    "address": "\tKi ốt Metro, Tổ 29 Khu 2,3 Phường Hà Tu, thành phố Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9546746,
    "Latitude": 107.1425442
  },
  {
    "STT": 42,
    "Name": "\tPhòng Chẩn Trị Dân An Đường\t",
    "address": "\tSố 2, ngõ 16 Tổ 1, khu 4, phường Hà Tu, Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9546746,
    "Latitude": 107.1425442
  },
  {
    "STT": 43,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Dân Tộc\t",
    "address": "\tTổ 1, khu 4, phường Hà Tu, Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9538859,
    "Latitude": 107.1528059
  },
  {
    "STT": 44,
    "Name": "\tNha Khoa Hạ Long\t",
    "address": "\tSố 308 đường Vũ Văn Hiếu, tổ 2, khu 5, P. Hà Tu, thành phố  Hạ Long; Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9619225,
    "Latitude": 107.1549008
  },
  {
    "STT": 45,
    "Name": "\tPhòng khám Răng Hàm Mặt Đức Oanh\t",
    "address": "\tSố nhà 01, ngõ 01, phố Minh Hà , P. Hà Tu, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9565441,
    "Latitude": 107.1608681
  },
  {
    "STT": 46,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\tTổ 22, khu 2, phường Hà Trung, thành phố  Hạ Long\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9579074,
    "Latitude": 107.0226806
  },
  {
    "STT": 47,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tTổ 14; khu II, Phường Hà Trung, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9618829,
    "Latitude": 107.1193331
  },
  {
    "STT": 48,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt\t",
    "address": "\tSố 30 phố Thương Mại, tổ 62, khu 2, Hồng Gai, Hạ Long Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9536125,
    "Latitude": 107.0687079
  },
  {
    "STT": 49,
    "Name": "\tPhòng khám Đông Y Thiện Tâm\t",
    "address": "\tSố 55 đường 25/4, phường Hồng Gai, thành phố, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9519872,
    "Latitude": 107.0771494
  },
  {
    "STT": 50,
    "Name": "\tPhòng khám Đa Khoa Bắc Sơn\t",
    "address": "\tSố 59, đường 25/4 tổ 10, khu 3, phường Hồng Gai, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.950689,
    "Latitude": 107.0790356
  },
  {
    "STT": 51,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Dân Tộc\t",
    "address": "\tSố nhà 57, đường 25/4, tổ 10, khu 3, phường Hồng Gai, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.950689,
    "Latitude": 107.0790356
  },
  {
    "STT": 52,
    "Name": "\tPhòng khám  Siêu Âm Ngoài Giờ\t",
    "address": "\tSố 209Tổ 7, Khu 3,  Hồng Hà; Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9417785,
    "Latitude": 107.1277751
  },
  {
    "STT": 53,
    "Name": "\tPhòng khám Minh Tâm\t",
    "address": "\tSố nhà 210 tổ 2, khu 4, Hồng Hà, Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9417785,
    "Latitude": 107.1277751
  },
  {
    "STT": 54,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tSố 146 Nguyễn Văn Cừ; P. Hồng Hà. thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9443086,
    "Latitude": 107.1225384
  },
  {
    "STT": 55,
    "Name": "\tPhòng khám Tư Nội Soi Tai, Mũi, Họng\t",
    "address": "\tTổ 6; khu 8; phường Hồng Hà; thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 56,
    "Name": "\t Nha Khoa Nam Việt\t",
    "address": "\tSố nhà 504, Nguyễn Văn Cừ, P. Hồng Hà, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9479461,
    "Latitude": 107.1295067
  },
  {
    "STT": 57,
    "Name": "\tPhòng khám Bệnh, Chữa Bệnh Hệ Nội Khoa \t",
    "address": "\tSố 419 Nguyễn Văn Cừ, P. Hồng Hà, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9479461,
    "Latitude": 107.1295067
  },
  {
    "STT": 58,
    "Name": "\tBác Sỹ Phạm Thị Nguyệt\t",
    "address": "\tSố nhà 80, Nguyễn Văn Cừ, tổ 2, khu I, P. Hồng Hà, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9413185,
    "Latitude": 107.116555
  },
  {
    "STT": 59,
    "Name": "\tPhòng khám Chuyên Siêu Âm Màu 4 Chiều\t",
    "address": "\tTổ 11, khu 6, P. Hồng Hà, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 60,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Tư Nhân\t",
    "address": "\tTổ 4 khu 7, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.951072,
    "Latitude": 107.134049
  },
  {
    "STT": 61,
    "Name": "\tCơ Sở Dịch Vụ Y Tế\t",
    "address": "\tTổ 5, khu 2, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.94086,
    "Latitude": 107.114717
  },
  {
    "STT": 62,
    "Name": "\tPhòng khám Nội Khoa Siêu Âm\t",
    "address": "\tSố nhà 621, đường Nguyễn Văn cừ, tổ 5, khu 7, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9456387,
    "Latitude": 107.1090998
  },
  {
    "STT": 63,
    "Name": "\tCơ Sở Ngọc Thái\t",
    "address": "\t Khu A, tầng 2, tòa nhà Licogi 18.1, tổ 11, khu 6, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9590927,
    "Latitude": 107.0728813
  },
  {
    "STT": 64,
    "Name": "\tDịch Vụ Y Tế Tư Nhân\t",
    "address": "\tSố nhà 383, đường Nguyễn Văn Cừ, P, Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9420345,
    "Latitude": 107.1208752
  },
  {
    "STT": 65,
    "Name": "\tPhòng khám Chuyên Khoa Nội Ngoài Giờ\t",
    "address": "\tSố nhà 12 đường Nguyễn Văn Cừ, phường Hồng Hà, thành phố  Hạ Long, tỉnh Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9419968,
    "Latitude": 107.1171927
  },
  {
    "STT": 66,
    "Name": "\tPhòng khám Hiền Nhi\t",
    "address": "\tSố nhà 82, tổ 2, khu 1, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 67,
    "Name": "\tPhòng khám Chuyên Khoa Mắt\t",
    "address": "\tSố nhà 564,đường Nguyễn Văn Cừ,  phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9420345,
    "Latitude": 107.1208752
  },
  {
    "STT": 68,
    "Name": "\tPhòng khám Đa Khoa Hoàng Anh\t",
    "address": "\t Số nhà 29 đường Nguyễn Văn Cừ, P. Hồng Hải, Hạ Long, , Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 69,
    "Name": "\tNha Khoa Đức Quảng\t",
    "address": "\t498 Nguyễn Văn Cừ, phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9483344,
    "Latitude": 107.1293807
  },
  {
    "STT": 70,
    "Name": "\tPhòng khám Sức Khoẻ Sinh Sản Đaị Phúc Khang\t",
    "address": "\tSố 647 Nguyễn Văn Cừ,Tổ 3, khu 7, P. Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.948042,
    "Latitude": 107.107703
  },
  {
    "STT": 71,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền \t",
    "address": "\t448 Nguyễn Văn Cừ - Phường Hồng Hải, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9525458,
    "Latitude": 107.1053065
  },
  {
    "STT": 72,
    "Name": "\tKính Mắt Hạ Long\t",
    "address": "\tSố nhà 133, khu 1A, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 73,
    "Name": "\tPhòng khám Ngoài Giờ Chuyên Khoa Tai Mũi Họng\t",
    "address": "\tTổ 3, khu 6A, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9500298,
    "Latitude": 107.1071861
  },
  {
    "STT": 74,
    "Name": "\tPhòng khám Siêu Âm \t",
    "address": "\tsố 202 Nguyễn Văn Cừ, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9412833,
    "Latitude": 107.1198026
  },
  {
    "STT": 75,
    "Name": "\tPhòng khám Chuyên Khoa Mắt Ngoài Giờ\t",
    "address": "\tSố 231 đường Nguyễn Văn Cừ; P. Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9481232,
    "Latitude": 107.1074036
  },
  {
    "STT": 76,
    "Name": "\tPhòng khám Chuyên Khoa Phụ Sản Bác Sỹ Luyến\t",
    "address": "\tTổ 2, khu I, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 77,
    "Name": "\tPhòng khám Bác Sỹ Bích\t",
    "address": "\tTổ 2, khu 4D, Phường Hồng Hải, Thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 78,
    "Name": "\tPhòng khám Bác Sỹ Minh Dương\t",
    "address": "\tSố nhà 314 Nguyễn Văn Cừ, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9517862,
    "Latitude": 107.1053642
  },
  {
    "STT": 79,
    "Name": "\tPhòng khám Chuyên Khoa Sản Thanh Xuân\t",
    "address": "\tSố nhà 823, Tổ 1; khu 10 - P. Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9958136,
    "Latitude": 106.9705427
  },
  {
    "STT": 80,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt 108 Ii\t",
    "address": "\tSố 230 Nguyễn Văn Cừ, P. Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9542838,
    "Latitude": 107.1021962
  },
  {
    "STT": 81,
    "Name": "\tPhòng khám Ngoài Giờ Sản Phụ Khoa Tâm Đức\t",
    "address": "\tSố nhà 76 Hải Sơn, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9542838,
    "Latitude": 107.1021962
  },
  {
    "STT": 82,
    "Name": "\tDịch Vụ Chăm Sóc Sức Khỏe Tại Nhà Hoàng Anh - Công Ty. Tnhh Chăm Sóc Sức Khỏe Hoàng Anh\t",
    "address": "\tSố nhà 13, Nguyễn Văn Cừ, Phường Hồng Hải, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9413764,
    "Latitude": 107.1142354
  },
  {
    "STT": 83,
    "Name": "\tNha Khoa Úc Châu\t",
    "address": "\tSố nhà 7, đường Nguyễn Văn Cừ,  phường Hồng Hải, thành phố  Hạ Long, tỉnh Quảng Ninh.\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9481232,
    "Latitude": 107.1074036
  },
  {
    "STT": 84,
    "Name": "\tPhòng khám Chữa Bệnh Phục Hồi Chức Năng\t",
    "address": "\tSố nhà 1B, ngõ 21, đường Nguyễn Văn Cừ, P. Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 85,
    "Name": "\tPhòng khám Chuyên Khoa Nhi\t",
    "address": "\tTổ 8, khu 8, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 86,
    "Name": "\tPhòng khám Sản Phụ Khoa Khhgđ - Trung Tâm Tư Vấn, Dịch Vụ Kế Hoạch Hóa Gia Đình, Chăm Sóc Sức Khỏe Sinh Sản \t",
    "address": "\tTổ 5, khu 12, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 87,
    "Name": "\tPhòng khám Sơn Dung\t",
    "address": "\tSố nhà 15, Phố Hải Long, tổ 1, khu 4A, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.8449115,
    "Latitude": 106.6880841
  },
  {
    "STT": 88,
    "Name": "\tPhòng khám Sản Phụ Khoa - Bs Mai Thanh Hằng\t",
    "address": "\tSố nhà 33, tổ 1, khu 6B,  phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 89,
    "Name": "\tPhòng khám Tai Mũi Họng Bảo Linh\t",
    "address": "\tSố nhà 54, tổ 3, khu 4D, phường Hồng Hải, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9499748,
    "Latitude": 107.1041465
  },
  {
    "STT": 90,
    "Name": "\tPhòng khám Bệnh, Chữa Bệnh Nội Khoa ( Bệnh Nghề Nghiệp) Thuộc Công Ty Tnhh Sức Khỏe Nghề Nghiệp Và Môi Trường Hoa Huệ\t",
    "address": "\tSố nhà 13, đường Nguyễn Văn Cừ, P.Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9557688,
    "Latitude": 107.0980769
  },
  {
    "STT": 91,
    "Name": "\tNha Khoa Quốc Tế 2\t",
    "address": "\tSố nhà 200, đường Nguyễn Văn Cừ, tổ 1, khu 4D, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9456387,
    "Latitude": 107.1090998
  },
  {
    "STT": 92,
    "Name": "\tPhòng khám Đa Khoa \t",
    "address": "\tTổ 3A, khu I, Phường Hùng Thắng, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9724879,
    "Latitude": 107.0111315
  },
  {
    "STT": 93,
    "Name": "\tPhòng khám Đa Khoa\t",
    "address": "\tTổ 3A, khu I, Phường Hùng Thắng, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9724879,
    "Latitude": 107.0111315
  },
  {
    "STT": 94,
    "Name": "\tRăng Giả Gia Truyền Hoàng Dũng\t",
    "address": "\tSố nhà 10, tổ 2, khu 5, P.Trần Hưng Đạo,thành phố Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9536176,
    "Latitude": 107.0854666
  },
  {
    "STT": 95,
    "Name": "\tPhòng khám Đa Khoa Y Cao Hà Nội\t",
    "address": "\tSố 6 Trần Hưng Đạo, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9536176,
    "Latitude": 107.0854666
  },
  {
    "STT": 96,
    "Name": "\tPhòng khám Răng Hàm Mặt Ngoài Giờ\t",
    "address": "\tSố 15, tổ 10, khu I - P. Trần Hưng Đạo. thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9545064,
    "Latitude": 107.0856982
  },
  {
    "STT": 97,
    "Name": "\tThẩm Mỹ Quỳnh Mai\t",
    "address": "\tTổ 8; khu 4; Trần Hưng Đạo; thành phố  Hạ Long; Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9539806,
    "Latitude": 107.085231
  },
  {
    "STT": 98,
    "Name": "\tNha Khoa Quốc Tế 3\t",
    "address": "\tSố nhà 14 , tổ 1, khu 1, P. Trần Hưng Đạo, thành phố, Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 99,
    "Name": "\tNha Khoa Hà Nội\t",
    "address": "\tSố nhà 62 , tổ 10, khu 1, P. Trần Hưng Đạo, thành phố, Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 100,
    "Name": "\tPhòng khám Chuyên Khoa Mắt\t",
    "address": "\tTổ 1, khu 3, P. Trần Hưng Đạo, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9539806,
    "Latitude": 107.085231
  },
  {
    "STT": 101,
    "Name": "\tPhòng khám Chữa Bệnh Nội Khoa\t",
    "address": "\tSố nhà 45, tổ 2, khu 2, P. Yết Kiêu, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9609997,
    "Latitude": 107.0796191
  },
  {
    "STT": 102,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tSố nhà 124, tổ 8, khu 3, P. Yết Kiêu, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9609997,
    "Latitude": 107.0796191
  },
  {
    "STT": 103,
    "Name": "\tPhòng khám Vạn Hoa Đường\t",
    "address": "\tSố nhà 36 phố Lê Lai, P. Yết Kiêu, thành phố  Hạ Long, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9606862,
    "Latitude": 107.0815703
  },
  {
    "STT": 104,
    "Name": "\tKhám Chữa Bệnh Chuyên Khoa Nhi\t",
    "address": "\tSố nhà 5, tổ 1, khu 2, phường Yết Kiêu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9533239,
    "Latitude": 107.0871119
  },
  {
    "STT": 105,
    "Name": "\tPhòng khám Sản Phụ Khoa - Siêu Âm Màu 4 Chiều\t",
    "address": "\tSố nhà 437, tổ 6, khu 2, phường Yết Kiêu, thành phố Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9600653,
    "Latitude": 107.0760905
  },
  {
    "STT": 106,
    "Name": "\tPhòng khám Chuyên Khoa Nhi\t",
    "address": "\tTổ 5, khu 6, Phường Yết Kiêu, thành phố  Hạ Long, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.96341,
    "Latitude": 107.0752
  },
  {
    "STT": 107,
    "Name": "\tPhòng khám Răng Hàm Mặt Việt Hưng\t",
    "address": "\tTổ 5, khu 11, phường Việt Hưng, thành phố  Hạ Long, tỉnh Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9959145,
    "Latitude": 106.9653756
  },
  {
    "STT": 108,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tÔ 16, Lô 8, khu đô thị Tây Cầu Trới, H. Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0238407,
    "Latitude": 106.9877208
  },
  {
    "STT": 109,
    "Name": "\tPhòng khám Nội Có Siêu Âm\t",
    "address": "\tTổ 5, khu 10, thị trấn Trới, H. Hoành Bồ Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0396647,
    "Latitude": 106.9889904
  },
  {
    "STT": 110,
    "Name": "\tCơ Sở  Thiện Phúc Đường\t",
    "address": "\tTổ 4; Khu 7, Thị trấn Trới, H. Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0256819,
    "Latitude": 106.9983133
  },
  {
    "STT": 111,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tKhu 1, Thị trấn Trới, Hoành Bồ, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0173987,
    "Latitude": 106.9911583
  },
  {
    "STT": 112,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt Hải Hậu\t",
    "address": "\tTổ 4, khu 3, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0396647,
    "Latitude": 106.9889904
  },
  {
    "STT": 113,
    "Name": "\tPhòng khám Viện Lan\t",
    "address": "\tTổ 2, khu 3, thị trấn Trới, H. Hoành Bồ, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0131278,
    "Latitude": 106.9939345
  },
  {
    "STT": 114,
    "Name": "\tPhòng khám Bệnh Ngoài Giờ Hằng Loan\t",
    "address": "\t114 Lê Lợi, P.  Quảng Yên, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.938619,
    "Latitude": 106.801413
  },
  {
    "STT": 115,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tPhố Nguyễn Du, khu 3, p. Quảng Yên, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9396254,
    "Latitude": 106.8089885
  },
  {
    "STT": 116,
    "Name": "\tPhòng khám Bệnh 1A Quang Trung \t",
    "address": "\tSố 1A Quang Trung, P. Quảng Yên, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9389369,
    "Latitude": 106.8001572
  },
  {
    "STT": 117,
    "Name": "\tPhòng khám Sản Phụ Khoa Tư Nhân\t",
    "address": "\tSố 6, Quang Trung, P. Quảng Yên, Thị xã Quảng Yên, QN\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9387415,
    "Latitude": 106.796489
  },
  {
    "STT": 118,
    "Name": "\tPhòng khám Chuyên Khoa Tai Mũi Họng\t",
    "address": "\t254 Trần Khánh Dư, Thị trấn Quảng Yên, Thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9390607,
    "Latitude": 106.8096251
  },
  {
    "STT": 119,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt\t",
    "address": "\tPhố Hồ xuân Hương- khu 9 , P.Quảng Yên, thị xã Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9387503,
    "Latitude": 106.795907
  },
  {
    "STT": 120,
    "Name": "\tPhòng khám Chuyên Khoa  Nội Bs Khánh Chung\t",
    "address": "\tSố 57 Ngô Quyền , thị trấnr Quảng Yên, thị xã  Quảng Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9397178,
    "Latitude": 106.802691
  },
  {
    "STT": 121,
    "Name": "\tPhòng khám Chuyên Khoa Phụ Sản - Kế Hoạch Hóa Gia Đình\t",
    "address": "\tKhu 9, Phường Quảng Yên, thị xã Quảng Yên, Quảng Ninh.\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9396254,
    "Latitude": 106.8089885
  },
  {
    "STT": 122,
    "Name": "\tNha Khoa Chí Hương, Khám Chữa Bệnh Rhm - Trồng Răng\t",
    "address": "\tSố 254 Trần Khánh Dư, P. Quảng Yên, thị xã  Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9385248,
    "Latitude": 106.8034708
  },
  {
    "STT": 123,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tSố nhà 39, phố Hoàng Hoa Thám, P. Quảng Yên, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.944094,
    "Latitude": 106.804926
  },
  {
    "STT": 124,
    "Name": "\tPhòng khám Răng Hàm Mặt Đoàn Thiện\t",
    "address": "\tKhu Khe Cát, phường Minh Thành, thị xã  Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9939794,
    "Latitude": 106.8709404
  },
  {
    "STT": 125,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Lê Văn Sâm\t",
    "address": "\tKhu 5; Phường Nam Hòa; Thị xã Quảng Yên; Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9308187,
    "Latitude": 106.8216995
  },
  {
    "STT": 126,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\tKhu 4, phường Phong Hải, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9089731,
    "Latitude": 106.8266873
  },
  {
    "STT": 127,
    "Name": "\tPhòng khám Nha Khoa Hạnh Dung\t",
    "address": "\tSố nhà 76 Trần Khánh Dư, khu 8, phường Quảng Yên, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.9396254,
    "Latitude": 106.8089885
  },
  {
    "STT": 128,
    "Name": "\tPhòng khám Vietnam Life\t",
    "address": "\tSố 24 Lê Lợi, P.Quảng Yên, thị xã Quảng Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.938619,
    "Latitude": 106.801413
  },
  {
    "STT": 129,
    "Name": "\tPhòng khám Siêu Âm - Bác Sỹ Mai Vũ Khiêm\t",
    "address": "\tTổ 3, Tre Mai, Phường Nam Khê, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0240197,
    "Latitude": 106.8091443
  },
  {
    "STT": 130,
    "Name": "\tPhòng Chẩn Trị Đông Y\t",
    "address": "\tSố 56 tiểu khu Cầu Sến, Phường Phương Đông , thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0352525,
    "Latitude": 106.7409083
  },
  {
    "STT": 131,
    "Name": "\tDịch Vụ Y Tế Phương Đông\t",
    "address": "\tBí Trung II, Phương Đông, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0377115,
    "Latitude": 106.731591
  },
  {
    "STT": 132,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tBí Trung II, Phương Đông, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0377115,
    "Latitude": 106.731591
  },
  {
    "STT": 133,
    "Name": "\tDịch Vụ Y Tế Tư Nhân\t",
    "address": "\tTổ 43, Khu 12, Phường Quang Trung, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0343187,
    "Latitude": 106.7754525
  },
  {
    "STT": 134,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tTổ 9, Khu 3, Quang Trung, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0342386,
    "Latitude": 106.7756671
  },
  {
    "STT": 135,
    "Name": "\tPhòng Chẩn Trị Y  Học Cổ Truyền\t",
    "address": "\tSố 246 Tổ 25B, khu 7, Phường Quang Trung, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0355884,
    "Latitude": 106.773595
  },
  {
    "STT": 136,
    "Name": "\tDịch Vụ Làm Răng Giả Đức An\t",
    "address": "\tSố 311 tổ 24 phường Quang Trung; thành phố  Uông Bí; Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0355884,
    "Latitude": 106.773595
  },
  {
    "STT": 137,
    "Name": "\tPhòng khám Nha Khoa Việt Đức\t",
    "address": "\tSố 61 Nguyễn Du, Phường Quang Trung, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0378074,
    "Latitude": 106.7701049
  },
  {
    "STT": 138,
    "Name": "\tPhòng khám Siêu Âm\t",
    "address": "\tSố 4, phố Thương Mại, phường Quang Trung ; Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0332523,
    "Latitude": 106.7695463
  },
  {
    "STT": 139,
    "Name": "\tPhòng khám Chiến Nga\t",
    "address": "\tSố 263 đường Quang Trung, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 140,
    "Name": "\tPhòng khám Chiến Nga 2\t",
    "address": "\tSố 263 đường Quang Trung, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 141,
    "Name": "\tNha Khoa Việt Pháp\t",
    "address": "\tSố 90, đường Quang Trung, P. Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 142,
    "Name": "\tPhòng khám Nha Khoa Quốc Tế Tlt\t",
    "address": "\tSố nhà 491, đường Quang Trung, P. Quang Trung, thành phố  Uông Bí , tỉnh Quảng NInh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 143,
    "Name": "\tKính Thuốc Minh Vương\t",
    "address": "\tSố nhà 79, tổ 19, khu 6, phường Quang Trung, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.1057847,
    "Latitude": 106.8075136
  },
  {
    "STT": 144,
    "Name": "\tKính Thuốc Thiên Hương\t",
    "address": "\tSố nhà 486, đường Quang Trung, phường Quang Trung, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0355884,
    "Latitude": 106.773595
  },
  {
    "STT": 145,
    "Name": "\tKính Mắt Đặng Hiền\t",
    "address": "\tSố nhà 496, khu 8, đường Quang Trung, phường Quang Trung, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0340907,
    "Latitude": 106.7756767
  },
  {
    "STT": 146,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\t732 Trần Nhân Tông, phường Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.035472,
    "Latitude": 106.7407238
  },
  {
    "STT": 147,
    "Name": "\tPhòng khám Nội Soi Tai Mũi Họng\t",
    "address": "\tNgõ 95 đường Tuệ Tĩnh, Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0400125,
    "Latitude": 106.7521023
  },
  {
    "STT": 148,
    "Name": "\tNha Khoa Xuân Thu\t",
    "address": "\tSố nhà 29 tổ 1, khu 3, Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.098618,
    "Latitude": 106.7927658
  },
  {
    "STT": 149,
    "Name": "\tPhòng khám Chuyên Khoa Mắt \t",
    "address": "\tSố 71 Tuệ Tĩnh, P. Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0386952,
    "Latitude": 106.7517406
  },
  {
    "STT": 150,
    "Name": "\tPhòng khám Sản Phụ Khoa Có Siêu Âm \t",
    "address": "\tTổ 1, khu I, phường Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.036461,
    "Latitude": 106.781601
  },
  {
    "STT": 151,
    "Name": "\tPhòng khám Nha Khoa Thế Kỷ\t",
    "address": "\tSố 570  Trần Nhân Tông, P. Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0346601,
    "Latitude": 106.7508829
  },
  {
    "STT": 152,
    "Name": "\tPhòng khám Nội Tuệ Tĩnh\t",
    "address": "\tTầng 2, số 124A Đường Tuệ Tĩnh, Thanh Sơn, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0374998,
    "Latitude": 106.7516414
  },
  {
    "STT": 153,
    "Name": "\tCơ Sở  Dịch Vụ Y Tế Hồng Chanh\t",
    "address": "\t466, Đường Trần Nhân Tông, Thanh Sơn, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0355119,
    "Latitude": 106.7431975
  },
  {
    "STT": 154,
    "Name": "\tPhòng khám Tai Mũi Họng  Thiện Tâm\t",
    "address": "\tSố 318 Trần Nhân Tông , P. Thanh Sơn, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0354071,
    "Latitude": 106.7426634
  },
  {
    "STT": 155,
    "Name": "\tPhòng khám Nhi\t",
    "address": "\tSố 20 Đường Tuệ Tĩnh, P. Thanh Sơn, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.035716,
    "Latitude": 106.7516212
  },
  {
    "STT": 156,
    "Name": "\tPhòng khám Bệnh 646 (Khám Nội)\t",
    "address": "\tSố nhà 646 Trần Nhân Tông, Phường Thanh Sơn; thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0348625,
    "Latitude": 106.7487232
  },
  {
    "STT": 157,
    "Name": "\tPhòng khám Bệnh 646 (Chẩn Đoán Hình Ảnh)\t",
    "address": "\tSố nhà 646 Trần Nhân Tông, Phường Thanh Sơn; thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0348625,
    "Latitude": 106.7487232
  },
  {
    "STT": 158,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền\t",
    "address": "\tSố nhà 145 tổ 3 khu 11, phường Thanh Sơn, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0521108,
    "Latitude": 106.7529514
  },
  {
    "STT": 159,
    "Name": "\tPhòng khám Nha Khoa Huy Hoàng\t",
    "address": "\tSố nhà 02, đường Tuệ Tĩnh, P. Thanh Sơn, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0374998,
    "Latitude": 106.7516414
  },
  {
    "STT": 160,
    "Name": "\tPhòng khám  Bs Cki Trần Thùy Dương\t",
    "address": "\tPhố Hữu Nghị, tổ 3, khu 7, P. Thanh Sơn, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0521108,
    "Latitude": 106.7529514
  },
  {
    "STT": 161,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tKiốt số 11 chợ Trưng Vương, P. Trưng Vương, thành phố  Uông Bí; Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0344082,
    "Latitude": 106.7855228
  },
  {
    "STT": 162,
    "Name": "\tPhòng khám Nhi Ngoài Giờ\t",
    "address": "\tTổ 12, khu II, Phường Trưng Vương, thành phố  Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0338005,
    "Latitude": 106.791291
  },
  {
    "STT": 163,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt Đức Hùng\t",
    "address": "\t85 đường Trần Nhân Tông, P. Yên Thanh, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0344942,
    "Latitude": 106.7517934
  },
  {
    "STT": 164,
    "Name": "\tPhòng khám Trí Thành\t",
    "address": "\tSố nhà 269 Trần Nhân Tông, Phường Yên Thanh, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0335982,
    "Latitude": 106.7590288
  },
  {
    "STT": 165,
    "Name": "\tPhòng khám Trí Thành\t",
    "address": "\tSố nhà 269 Trần Nhân Tông, Phường Yên Thanh, Uông Bí, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0335982,
    "Latitude": 106.7590288
  },
  {
    "STT": 166,
    "Name": "\tPhòng khám Răng Hàm Mặt Ngoài Giờ\t",
    "address": "\tSố 275, tổ 7, khu I, phường Yên Thanh, Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0337507,
    "Latitude": 106.7571025
  },
  {
    "STT": 167,
    "Name": "\tPhòng khám Đa Khoa Đông Đô \t",
    "address": "\tSố 315 Cầu Sến, phường Yên Thanh, thành phố Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0352525,
    "Latitude": 106.7409083
  },
  {
    "STT": 168,
    "Name": "\tPhòng khám 416 - Bác Sỹ Nhàn\t",
    "address": "\tKhu 8, phường Vàng Danh, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.1102653,
    "Latitude": 106.8060388
  },
  {
    "STT": 169,
    "Name": "\tPhòng khám Phạm Thị Hồng Vân\t",
    "address": "\tSố 15, ngõ 124, tổ 2, khu An Hải, Phường Phương Nam, thị xã Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.030896,
    "Latitude": 106.723961
  },
  {
    "STT": 170,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\tTổ 5, Vĩnh Xuân, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0598846,
    "Latitude": 106.6057169
  },
  {
    "STT": 171,
    "Name": "\tPhòng khám Nội Có Siêu Âm\t",
    "address": "\tKhu Vĩnh Tuy I, Thị trấn Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 172,
    "Name": "\tDịch Vụ Y Tế Phi Phượng\t",
    "address": "\tTổ 1, Khu Vĩnh Hòa, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.060623,
    "Latitude": 106.584913
  },
  {
    "STT": 173,
    "Name": "\tPhòng khám Sản Phụ Khoa Có Siêu Âm\t",
    "address": "\tTổ 5, Khu Vĩnh Xuân, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0598846,
    "Latitude": 106.6057169
  },
  {
    "STT": 174,
    "Name": "\tPhòng Chẩn Trị Yhct  Đức Hội\t",
    "address": "\tSố 24 Hoàng Hoa Thám, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0572629,
    "Latitude": 106.5966365
  },
  {
    "STT": 175,
    "Name": "\tPhòng khám Nội Siêu Âm\t",
    "address": "\t207 Đường Hoàng Thạch, Vĩnh Tuy 2, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.039962,
    "Latitude": 106.585229
  },
  {
    "STT": 176,
    "Name": "\tPhòng khám Nha Khoa Thẩm Mỹ Hải Oanh\t",
    "address": "\tKhu Vĩnh Thông, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.056883,
    "Latitude": 106.598092
  },
  {
    "STT": 177,
    "Name": "\tPhòng khám Chuyên Khoa Siêu Âm\t",
    "address": "\tKhu Vĩnh Quang , Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0608474,
    "Latitude": 106.5839112
  },
  {
    "STT": 178,
    "Name": "\tPhòng khám Nội Khoa\t",
    "address": "\tSố nhà 44, phố II, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0675098,
    "Latitude": 106.5996595
  },
  {
    "STT": 179,
    "Name": "\tPhòng khám  Răng Hàm Mặt  Minh Trang\t",
    "address": "\tKhu Công Nông, thị trấn  Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 180,
    "Name": "\tPhòng khám Hồng Hà\t",
    "address": "\tSố 182- Hoàng Hoa Thám, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0574879,
    "Latitude": 106.6018596
  },
  {
    "STT": 181,
    "Name": "\tPhòng khám Chuyên Khoa X- Quang\t",
    "address": "\tKhu Vĩnh Lập, Mạo Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0684823,
    "Latitude": 106.5876389
  },
  {
    "STT": 182,
    "Name": "\tTrồng Răng Tân Tiến\t",
    "address": "\tSố nhà 171 Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 183,
    "Name": "\tPhòng khám Răng Hàm Mặt Tâm Đức 1\t",
    "address": "\tSố nhà 97, Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 184,
    "Name": "\tDịch Vụ Y Tế Minh Ngọc\t",
    "address": "\tSố nhà 23 khu phố 2, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 185,
    "Name": "\tPhòng khám Đa Khoa Hoàng Anh Ii\t",
    "address": "\tSố nhà 151 Hoàng Hoa Thám, phường Mạo Khê, thị xã Đông Triều, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0584203,
    "Latitude": 106.5985501
  },
  {
    "STT": 186,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền \t",
    "address": "\t82 Chợ Cột, thị trấn Đông Triều, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0836378,
    "Latitude": 106.5161416
  },
  {
    "STT": 187,
    "Name": "\tPhòng khám Nội Ngoài Giờ\t",
    "address": "\tSố nhà 95, Trần Nhân Tông,thị trấnr Đông Triều, Huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0797764,
    "Latitude": 106.5145667
  },
  {
    "STT": 188,
    "Name": "\tPhòng khám Phụ Sản, Tư Vấn Sức Khoẻ Sinh Sản\t",
    "address": "\tSố 263 khu 4 thị trấn Đông Triều, huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.087021,
    "Latitude": 106.516197
  },
  {
    "STT": 189,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tTổ 8, Khu I, Thị trấn Đông Triều, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0958153,
    "Latitude": 106.6055534
  },
  {
    "STT": 190,
    "Name": "\tPhòng Chẩn Trị Y  Học Cổ Truyền\t",
    "address": "\tSố 191 phố Trần Nhân Tông; Thị trấn Đông Triều; H. Đông Triều; Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0797764,
    "Latitude": 106.5145667
  },
  {
    "STT": 191,
    "Name": "\tBác Sỹ Nguyễn Trọng Yên - Chuyên Khoa Răng Hàm Mặt\t",
    "address": "\tSố nhà 111, Phố Trần Nhân Tông, khu 3, thị trấn Đông Triều, H. Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0840301,
    "Latitude": 106.5163253
  },
  {
    "STT": 192,
    "Name": "\tPhòng khám Sản Phụ Khoa - Bs Khay\t",
    "address": "\tĐầu Đình, Đức Chính, Thị trấn Đông Triều, Huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0897049,
    "Latitude": 106.5187296
  },
  {
    "STT": 193,
    "Name": "\tPhòng khám Nam Hải\t",
    "address": "\tSố nhà 118, phố Nguyễn Bình, thị trấnr Đông Triều, Đông Triều, QN\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0832498,
    "Latitude": 106.511486
  },
  {
    "STT": 194,
    "Name": "\tPhòng khám Nội Ngoài Giờ\t",
    "address": "\tThôn Bình Sơn Đông, xã Bình Dương, Đông Triều\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.1146088,
    "Latitude": 106.4817667
  },
  {
    "STT": 195,
    "Name": "\tDịch Vụ Y Tế Phương Nga\t",
    "address": "\tThôn Bình Sơn Đông, xã Bình Dương, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.1146088,
    "Latitude": 106.4817667
  },
  {
    "STT": 196,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tThôn Trại Thông, xã Bình Khê, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.104795,
    "Latitude": 106.585353
  },
  {
    "STT": 197,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tThôn Trạo Hà, xã Đức Chính, Đông Triều,Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0769616,
    "Latitude": 106.5228011
  },
  {
    "STT": 198,
    "Name": "\tNha Khoa Thẩm Mỹ Hoàng Long\t",
    "address": "\tThôn 1, xã Đức Chính, huyện Đông Triều, tỉnh Quảng NInh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0797764,
    "Latitude": 106.5145667
  },
  {
    "STT": 199,
    "Name": "\tPhòng khám Bệnh Ngoại Khoa \t",
    "address": "\tThôn Mễ Xá I, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 200,
    "Name": "\tPhòng khám Chuyên Khoa Phụ Sản\t",
    "address": "\tThôn Mễ Xá II, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 201,
    "Name": "\tPhòng khám Y Tư Nhân\t",
    "address": "\tThôn Mễ Xá II, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 202,
    "Name": "\tPhòng khám Nội \t",
    "address": "\tThôn Mễ xá 3, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 203,
    "Name": "\tDịch Vụ Y Tế Thu Hằng\t",
    "address": "\tThôn Mễ Xá I, Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 204,
    "Name": "\tPhòng khám 103  Hà Nội\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 205,
    "Name": "\tPhòng khám 103 Hà Nội\t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0727648,
    "Latitude": 106.5252636
  },
  {
    "STT": 206,
    "Name": "\tPhòng khám Siêu Âm\t",
    "address": "\tMễ Xá II - xã Hưng Đạo - Huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0704796,
    "Latitude": 106.5245158
  },
  {
    "STT": 207,
    "Name": "\tPhòng khám 103 Hà Nội \t",
    "address": "\tThôn Mễ Xá, xã Hưng Đạo, Huyện Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 208,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tThôn Mễ Xá1, Hưng Đạo, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.064797,
    "Latitude": 106.5268278
  },
  {
    "STT": 209,
    "Name": "\tPhòng khám  Khải Ngần\t",
    "address": "\tQuán Cát, Thủy An, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.087021,
    "Latitude": 106.516197
  },
  {
    "STT": 210,
    "Name": "\tPhòng khám Siêu Âm Sản Phụ Khoa Ngoài Giờ, Khhgđ\t",
    "address": "\tXuân Cầm, Xuân Sơn, Đông Triều, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0759347,
    "Latitude": 106.5448619
  },
  {
    "STT": 211,
    "Name": "\tNha Khoa Mạnh Cường\t",
    "address": "\t145 Bà Triệu, Cẩm Đông, Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0069539,
    "Latitude": 107.2923219
  },
  {
    "STT": 212,
    "Name": "\tPhòng khám Chữa Bệnh Hà Thảo\t",
    "address": "\tSố 139, đường Bà Triệu, Cẩm Đông, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0081551,
    "Latitude": 107.2919065
  },
  {
    "STT": 213,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền\t",
    "address": "\tSố 5 Phố Bà Triệu, P. Cẩm Đông, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0063527,
    "Latitude": 107.2924214
  },
  {
    "STT": 214,
    "Name": "\tPhòng Chẩn Trị Tín Đường\t",
    "address": "\tTổ 38 khu Đông Tiến 1 , Phường Cẩm Đông, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0086786,
    "Latitude": 107.294886
  },
  {
    "STT": 215,
    "Name": "\tPhòng khám Chuyên Khoa Phụ Sản\t",
    "address": "\tKhu 8, Phường Cửa Ông, thành phố Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0563077,
    "Latitude": 107.3527336
  },
  {
    "STT": 216,
    "Name": "\tDịch Vụ Y Tế Nhung Hương\t",
    "address": "\tTổ 43, Khu 4B1, Phường Cửa Ông, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 217,
    "Name": "\tPhòng khám Đa Khoa Trường Phúc\t",
    "address": "\tSố 489, Tổ 89, khu 7, P.Cửa Ông, Cẩm Phả, Quảng Ninh \t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 218,
    "Name": "\tPhòng khám Nha Khoa Hồng Hà\t",
    "address": "\tSố nhà 360, Tổ 64, khu 6, phố Mới, phường Cửa Ông, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.032741,
    "Latitude": 107.3598781
  },
  {
    "STT": 219,
    "Name": "\tPhòng khám Nha Khoa 3D\t",
    "address": "\tSố nhà 484, đường Lý Thường Kiệt, tổ 80, khu 6, phường Cửa Ông, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0217791,
    "Latitude": 107.3652831
  },
  {
    "STT": 220,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt Việt Mỹ\t",
    "address": "\tSố 490, tổ 109,khu 3B, P. Cẩm Phú, Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.015757,
    "Latitude": 107.3316667
  },
  {
    "STT": 221,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Tư Nhân\t",
    "address": "\tSố 554, tổ 108, khu 8B, P.Cẩm Phú, Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.015757,
    "Latitude": 107.3316667
  },
  {
    "STT": 222,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tSố nhà 442 Tổ 30, khu 2B, Phường Cẩm Phú, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.015757,
    "Latitude": 107.3316667
  },
  {
    "STT": 223,
    "Name": "\tPhòng khám Nội Có Siêu Âm\t",
    "address": "\tTổ 30, phường Cẩm Phú, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.00983,
    "Latitude": 107.337
  },
  {
    "STT": 224,
    "Name": "\tPhòng khám Nội Bs Minh Tâm\t",
    "address": "\tKi ôt số 1, đường 12/11 phường Cẩm Phú, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010771,
    "Latitude": 107.291028
  },
  {
    "STT": 225,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền\t",
    "address": "\tTổ 100, khu 8, Phường Cẩm Phú, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.015757,
    "Latitude": 107.3316667
  },
  {
    "STT": 226,
    "Name": "\tPhòng khám Bệnh Phụ Khoa\t",
    "address": "\tTổ 3; khu Minh Tiến A, P. Cẩm Tây, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0322185,
    "Latitude": 107.2843726
  },
  {
    "STT": 227,
    "Name": "\tCơ Sở  Dịch Vụ Y Tế Tư Nhân\t",
    "address": "\tSố 64 Hồ Tùng Mậu, P. Cẩm Tây, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0133495,
    "Latitude": 107.287831
  },
  {
    "STT": 228,
    "Name": "\tPhòng khám Chữa Bệnh Ngoài Da Hoa Liễu\t",
    "address": "\tSố nhà 3, Tổ 5 Phường Cẩm Tây, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0006844,
    "Latitude": 107.3036619
  },
  {
    "STT": 229,
    "Name": "\tNha Khoa Vinh Quang\t",
    "address": "\tSố 43, tổ 6, khu Lê Hồng Phong, đường Trần Phú, P. Cẩm Tây, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0129841,
    "Latitude": 107.2824314
  },
  {
    "STT": 230,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Hoàng Loan\t",
    "address": "\tSố nhà 23, tổ 5, phường Cẩm Tây, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0322185,
    "Latitude": 107.2843726
  },
  {
    "STT": 231,
    "Name": "\tNha Khoa Quốc Tế Mỹ\t",
    "address": "\tSố nhà 29, đường Trần Phú, phường Cẩm Tây, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0129841,
    "Latitude": 107.2824314
  },
  {
    "STT": 232,
    "Name": "\tNha Khoa Y Đức Hà Nội\t",
    "address": "\t594 Trần Phú, Cẩm Thạch, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010249,
    "Latitude": 107.2587386
  },
  {
    "STT": 233,
    "Name": "\tPhòNg KháM Đa Khoa Y Cao Hà NộI\t",
    "address": "\tSố nhà 857, đường Trần Phú, phường Cẩm Thạch, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010068,
    "Latitude": 107.258502
  },
  {
    "STT": 234,
    "Name": "\tPhòng khám Răng Hàm Mặt Cường Trang\t",
    "address": "\tSố 6/206 Đường Trần Phú, Cẩm Thành, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 235,
    "Name": "\tPhòng khám Thắng Hà\t",
    "address": "\t264, Khu 4B, P. Cẩm Thành, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 236,
    "Name": "\tPhòng Xét Nghiệm Vương Minh Châu\t",
    "address": "\tTổ 4, khu 7, phường Cẩm Thành, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 237,
    "Name": "\tPhòng khám Chuyên Khoa Chẩn Đoán Hình Ảnh\t",
    "address": "\tTổ 6, Khu 7, phường Cẩm Thành, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 238,
    "Name": "\tPhòng khám Đa Khoa Y Đức Hà Nội\t",
    "address": "\t194 đường Trần Phú, P. Cẩm Thành, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0132148,
    "Latitude": 107.282327
  },
  {
    "STT": 239,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt Cali - Sài Gòn\t",
    "address": "\tSố 296 đường Trần Phú, phường Cẩm Thành, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0118793,
    "Latitude": 107.2790011
  },
  {
    "STT": 240,
    "Name": "\tPhòng khám Duy Thanh Siêu Âm Mầu 4D\t",
    "address": "\tSố 07, đường Thanh Niên, tổ 3, khu 6, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 241,
    "Name": "\tPhòng Chẩn Trị Yhct Phúc Lâm Đường\t",
    "address": "\tTổ 4, khu 4B, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0113744,
    "Latitude": 107.2769837
  },
  {
    "STT": 242,
    "Name": "\tPhòng khám Chuyên Khoa Ngoại\t",
    "address": "\tTổ 1, Khu 5B, Phường Cẩm Thịnh, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 20.995319,
    "Latitude": 107.3421183
  },
  {
    "STT": 243,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Ninh Thanh Đường\t",
    "address": "\tSố 260 Đường Trần Quốc Tảng, P. Cẩm Thịnh, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0069976,
    "Latitude": 107.3451774
  },
  {
    "STT": 244,
    "Name": "\tDịch Vụ Y Tế \t",
    "address": "\tTổ 1, khu 6B; Phường Cẩm Thịnh; thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0119009,
    "Latitude": 107.3494711
  },
  {
    "STT": 245,
    "Name": "\tPhòng khám Sản Phụ Khoa\t",
    "address": "\tTổ 7, khu 2, phường Cẩm Thịnh, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0057513,
    "Latitude": 107.3478674
  },
  {
    "STT": 246,
    "Name": "\tNha Khoa Hà Thành\t",
    "address": "\tSố 530 Đường Trần Phú, Cẩm Thủy, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0099349,
    "Latitude": 107.2683182
  },
  {
    "STT": 247,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tTổ 6, khu 2, Cẩm Thủy, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0141994,
    "Latitude": 107.2636842
  },
  {
    "STT": 248,
    "Name": "\tPhòng khám Nội\t",
    "address": "\t514 đường Trần Phú, P. Cẩm Thủy, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0098992,
    "Latitude": 107.2656074
  },
  {
    "STT": 249,
    "Name": "\tPhòng khám Răng Hà Nội\t",
    "address": "\tTổ 1, khu Tâm Lập 5, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0141994,
    "Latitude": 107.2636842
  },
  {
    "STT": 250,
    "Name": "\tPhòng khám Ck Tai Mũi Họng - Phòng khámđk Tư Nhân Cẩm Phả\t",
    "address": "\tSố 841 đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010141,
    "Latitude": 107.2589275
  },
  {
    "STT": 251,
    "Name": "\tPhòng khám Ck Da Liễu - Phòng khámđk Tư Nhân Cẩm Phả\t",
    "address": "\tSố 841 đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010141,
    "Latitude": 107.2589275
  },
  {
    "STT": 252,
    "Name": "\tPhòng khám Ck Mắt - Phòng khámđk Tư Nhân Cẩm Phả\t",
    "address": "\tSố 841 đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010141,
    "Latitude": 107.2589275
  },
  {
    "STT": 253,
    "Name": "\tPhòng khám Ck Rhm - Phòng khámđk Tư Nhân Cẩm Phả\t",
    "address": "\tSố 841 đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.010141,
    "Latitude": 107.2589275
  },
  {
    "STT": 254,
    "Name": "\tPhòng khámck Sản - Phòng khámđk Tư Nhân Cẩm Phả\t",
    "address": "\tSố nhà 481, đườn Trần Phú, phường cẩm Thủy, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0098992,
    "Latitude": 107.2656074
  },
  {
    "STT": 255,
    "Name": "\tPhòng khám Đa Khoa Tư Nhân Cẩm Phả\t",
    "address": "\tSố nhà 481, đường Trần Phú, phường Cẩm Thủy, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0098992,
    "Latitude": 107.2656074
  },
  {
    "STT": 256,
    "Name": "\tNha Khoa ViệT ĐứC Hà NộI\t",
    "address": "\tSố nhà 717, tổ 2, khu Tân Lập 6, P. cẩm Thủy, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0141994,
    "Latitude": 107.2636842
  },
  {
    "STT": 257,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tTổ 7, khu 4A, Phường Cẩm Trung, Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 258,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Tư Nhân\t",
    "address": "\tSố 42, tổ 59A, Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 259,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tSố 14, Đường Bái Tử Long, tổ 3, khu 4A, phường Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 260,
    "Name": "\tPhòng khám Chuyên Khoa Phụ Sản\t",
    "address": "\tSố 71 đường Bái Tử Long, Tổ 9; khu 3B, Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 261,
    "Name": "\tDịch Vụ Y Tế Tư Nhân\t",
    "address": "\tSố 170, tổ 4, khu 3A , p. Cẩm Trung , Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 262,
    "Name": "\tPhòng Chẩn Trị Đông Y\t",
    "address": "\tSố nhà 395, đường Trần Phú , Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0096427,
    "Latitude": 107.2711615
  },
  {
    "STT": 263,
    "Name": "\tPhòng khám Chữa Bệnh Dũng Mai - Chuyên Khoa Răng Hàm Mặt\t",
    "address": "\tSố 2 Đường Bái Tử Long, P. Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0053703,
    "Latitude": 107.2724365
  },
  {
    "STT": 264,
    "Name": "\tPhòng khám Nội Khoa Có Siêu Âm\t",
    "address": "\tSố nhà 32 tổ 5, khu 6A, Phường Cẩm Trung, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 265,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền \t",
    "address": "\tSố nhà 3, tổ 6, khu 4, phường Cẩm Trung, thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 266,
    "Name": "\tPhòng khám Nội Soi Tai Mũi Họng\t",
    "address": "\tTổ 6, khu 5A, phường Cẩm Trung,  thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 267,
    "Name": "\tPhòng khám Nha Khoa Á Châu\t",
    "address": "\tSố nhà 348 đường Trần Phú, phường Cẩm Trung, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0097891,
    "Latitude": 107.270649
  },
  {
    "STT": 268,
    "Name": "\tPhóNg KháM Tai MũI HọNg An Phú\t",
    "address": "\tSố nhà 14, tổ 5, khu 6A, P. Cẩm Trung , thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0020567,
    "Latitude": 107.269595
  },
  {
    "STT": 269,
    "Name": "\tDịch Vụ Y Tế Trung Nhung\t",
    "address": "\tsố nhà 332, Bạch Đằng 1, P. Phương Nam, thành phố  Uông Bí, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0233902,
    "Latitude": 106.6939803
  },
  {
    "STT": 270,
    "Name": "\tPhòng khám Nha Khoa Hà Thành 2\t",
    "address": "\tSố 424 đường Hoàng Quốc Việt,  tổ 3 khu An Sơn, P.Cẩm Sơn, thành phố Cẩm Phả, Quang Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.005601,
    "Latitude": 107.3115091
  },
  {
    "STT": 271,
    "Name": "\tPhòNg KháM Anh HàO\t",
    "address": "\tSố nhà 246, tổ 5, khu An Sơn,  P. Cẩm Sơn, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0006844,
    "Latitude": 107.3036619
  },
  {
    "STT": 272,
    "Name": "\tNha Khoa Mạnh Cường Ii\t",
    "address": "\tSố 70, tổ 1, khu I, Phường Mông Dương, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0822391,
    "Latitude": 107.3021069
  },
  {
    "STT": 273,
    "Name": "\tPhòng khám Nội Khoa Y Đức\t",
    "address": "\tTổ 1 khu 12 Phường Mông Dương, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.06284,
    "Latitude": 107.343399
  },
  {
    "STT": 274,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hàm Mặt 3A\t",
    "address": "\tTổ 5 khu 5, P. Mông Dương, thành phố  Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.065676,
    "Latitude": 107.32471
  },
  {
    "STT": 275,
    "Name": "\tPhòNg KháM NộI Anh Tú\t",
    "address": "\tTổ 8, khu 5, phường Mông Dương, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0822391,
    "Latitude": 107.3021069
  },
  {
    "STT": 276,
    "Name": "\tPhòng khám Nội Khoa Tư Nhân Ngoài Giờ\t",
    "address": "\tSố 287  Tổ 4, khu  3A, phường Quang Hanh, Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 277,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Dân Tộc\t",
    "address": "\tTổ 2, khu 6, Quang Hanh, Cẩm Phả\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0087029,
    "Latitude": 107.2348806
  },
  {
    "STT": 278,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tTổ 4, khu 9B, Phường Quang Hanh, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 279,
    "Name": "\tPhòng Chẩn Trị Y  Học Cổ Truyền\t",
    "address": "\tSố 340 khu 8, Quang Hanh, Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 280,
    "Name": "\tDịch Vụ Y Tế Hồng Lĩnh\t",
    "address": "\tSố 386 , Nguyễn Đức Cảnh,Tổ 3, khu 3B; Phường Quang Hanh; thành phố  Cẩm Phả, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0083343,
    "Latitude": 107.2283952
  },
  {
    "STT": 281,
    "Name": "\tPhòng khám Tri Ân\t",
    "address": "\tSố nhà 238 đường Nguyễn Đức cảnh, Tổ 10, khu 3A , P. Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0082107,
    "Latitude": 107.2271147
  },
  {
    "STT": 282,
    "Name": "\tPhòng khám Siêu Âm Bác Sỹ Nhan\t",
    "address": "\tSố nhà 35, Khu 4, thị trấn Cái Rồng, Huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 283,
    "Name": "\tPhòng khám Răng Hàm Mặt Nha Khoa Tdl\t",
    "address": "\tSN 98 khu 3, Thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 284,
    "Name": "\tPhòng khám Chuyên Khoa Răng Hà Nội - Vđ\t",
    "address": "\tKhu 2, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.0662347,
    "Latitude": 107.4230585
  },
  {
    "STT": 285,
    "Name": "\tPhòng khám Nha Khoa Độ Lượng\t",
    "address": "\tSố nhà 209 khu 5, thị trấn Cái Rồng, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.053875,
    "Latitude": 107.4351532
  },
  {
    "STT": 286,
    "Name": "\tPhòng khám Chuyên Khoa Chẩn Đoán Hình Ảnh Sáng Huệ\t",
    "address": "\tKhu 6, thị trấn Cái Rồng, huyện Vân Đồn, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.063355,
    "Latitude": 107.425806
  },
  {
    "STT": 287,
    "Name": "\tDịch Vụ Kính Thuốc Tâm Bình\t",
    "address": "\tSố 61, Đông Tiến, Thị trấn Tiên Yên, Huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3309419,
    "Latitude": 107.4054489
  },
  {
    "STT": 288,
    "Name": "\tPhòng  Khám Chuyên Khoa Xét Nghiệm  Tâm Phúc Hà Nội\t",
    "address": "\tPhố Đông Tiến 1 - Thị trấn Tiên Yên, Huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 289,
    "Name": "\tPhòng khám Chuyên Khoa Chẩn Đoán Hình Ảnh Tâm Phúc\t",
    "address": "\tPhố Đông Tiến 1 - Thị trấn Tiên Yên, Huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 290,
    "Name": "\tNha Khoa Tâm Đức\t",
    "address": "\tCổng số 1, Đường Mới, thị trấn Tiên Yên, Huyện Tiên Yên, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 291,
    "Name": "\tPhòng khám Đa Khoa Thiên Tân\t",
    "address": "\tSố 25, phố Lý Thường Kiệt, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3315865,
    "Latitude": 107.4025342
  },
  {
    "STT": 292,
    "Name": "\tPhòng khám Nha Khoa Thịnh Vượng\t",
    "address": "\tSố 229 phố Tống Nhất, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3347973,
    "Latitude": 107.3937536
  },
  {
    "STT": 293,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền - Hội Chũ Thập Đỏ Tỉnh Quảng Ninh\t",
    "address": "\tSố nhà 37, phố Thống Nhất, thị trấn Tiên Yên, huyện Tiên yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3289859,
    "Latitude": 107.3966896
  },
  {
    "STT": 294,
    "Name": "\tPhòng khám Chuyên Khoa Nội Tâm Phúc Hà Nội\t",
    "address": "\tPhố Đông Tiến 1, thị trấnr Tiên Yên, H. Tiên yên, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3284071,
    "Latitude": 107.3996674
  },
  {
    "STT": 295,
    "Name": "\tDịch Vụ Làm Răng Giả\t",
    "address": "\tSố 25 Hoàng Văn Thụ, thị trấnr Đầm Hà, Huyện Đầm Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.3517005,
    "Latitude": 107.5994217
  },
  {
    "STT": 296,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tSố 35 Chu Văn An, thị trấnr Quảng Hà, H.Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4501648,
    "Latitude": 107.7532579
  },
  {
    "STT": 297,
    "Name": "\tPhòng khám Tư Nhân Chuyên Khoa Răng Hàm Mặt Nha Khoa Trường Giang\t",
    "address": "\tSố nhà 26, phố Trần Bình Trọng, thị trấnr Quảng Hà, H. Hải Hà, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.452286,
    "Latitude": 107.7521534
  },
  {
    "STT": 298,
    "Name": "\tPhòng khám Bình Minh Hà Nội\t",
    "address": "\tSố 4, Trần Bình Trọng, thị trấnr Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4508305,
    "Latitude": 107.7460583
  },
  {
    "STT": 299,
    "Name": "\tPhòng khám Bình Minh Hà Nội\t",
    "address": "\tSố 4, Trần Bình Trọng, thị trấnr Quảng Hà, huyện Hải Hà, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4508305,
    "Latitude": 107.7460583
  },
  {
    "STT": 300,
    "Name": "\tNha Khoa Hà Nội Hc\t",
    "address": "\tSố 35, phố Lý Thường Kiệt, thị trấn Quảng Hà, huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4531035,
    "Latitude": 107.759225
  },
  {
    "STT": 301,
    "Name": "\tPhòng Chẩn Trị Yhct Đình Hưng Đường\t",
    "address": "\t Số B3, khu Ki ốt B chợ trung tâm Hải Hà, thị trấn Quảng Hà, huyện Hải Hà, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4498682,
    "Latitude": 107.7493787
  },
  {
    "STT": 302,
    "Name": "\tPhòng khám Ngoài Giờ\t",
    "address": "\tThôn Minh Tân, xã Quảng Minh, Hải Hà, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4182232,
    "Latitude": 107.71625
  },
  {
    "STT": 303,
    "Name": "\tPhòng khám Ngoài Giờ\t",
    "address": "\tThôn Minh Tân, xã Quảng Minh, Hải Hà, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4182232,
    "Latitude": 107.71625
  },
  {
    "STT": 304,
    "Name": "\tDịch Vụ Y Tế Tư Nhân\t",
    "address": "\tKhu 3, phường Hải Hoà, Móng Cái\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5188202,
    "Latitude": 108.0179527
  },
  {
    "STT": 305,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tKhu 4, phường Hải Hòa, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.532994,
    "Latitude": 107.9674392
  },
  {
    "STT": 306,
    "Name": "\tPhòng khám Chuyên Khoa Nội\t",
    "address": "\tKhu 4, phường Hải Hòa, thành phố  Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.532994,
    "Latitude": 107.9674392
  },
  {
    "STT": 307,
    "Name": "\tPhòng khám Và Chữa Bệnh Nội Khoa Tư Nhân\t",
    "address": "\tKhu 8, đường Hùng Vương, phường Hải Hòa, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5294265,
    "Latitude": 107.9682658
  },
  {
    "STT": 308,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tSố nhà 70 Hoà Lạc, P. Hoà Lạc,thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5277644,
    "Latitude": 107.9694061
  },
  {
    "STT": 309,
    "Name": "\tPhòng khám Bệnh Nội Khoa\t",
    "address": "\tSố 72 phố Hoà Lạc, phường Hoà Lạc, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.524528,
    "Latitude": 107.9665168
  },
  {
    "STT": 310,
    "Name": "\tNha Khoa Thanh Xuân Hà Nội\t",
    "address": "\tSố 79 Lý Tự Trọng, Phường Hoà Lạc, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5270026,
    "Latitude": 107.9707789
  },
  {
    "STT": 311,
    "Name": "\tPhòng khám Chuyên Khoa Nội Ngoài Giờ\t",
    "address": "\tSố 8 Đào Phúc Lộc, Hòa Lạc, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5280605,
    "Latitude": 107.9692715
  },
  {
    "STT": 312,
    "Name": "\tNha Khoa Việt Nhật 2\t",
    "address": "\tSố 4, đường Hùng Vương, phường Hòa Lạc, thành phố  Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5278509,
    "Latitude": 107.9703632
  },
  {
    "STT": 313,
    "Name": "\tPhòng khám Đa Khoa Diên Hồng Y Đức\t",
    "address": "\t144B Hùng Vương, Phương Ka Long, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.531239,
    "Latitude": 107.9596089
  },
  {
    "STT": 314,
    "Name": "\tPhòng khám Nội Khoa Tư Nhân\t",
    "address": "\tSố 10 đường Tuệ Tĩnh,Phường Ka Long, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5251109,
    "Latitude": 107.9622663
  },
  {
    "STT": 315,
    "Name": "\tPhòng khám Tư Nhân Răng Hàm Mặt\t",
    "address": "\tSố 35B Hùng Vương; Ka Long; thành phố  Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5307754,
    "Latitude": 107.9622546
  },
  {
    "STT": 316,
    "Name": "\tPhòng khám Bệnh Nội Khoa\t",
    "address": "\tSố 23 Hùng Vương, Ka Long, thành phố  Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5307359,
    "Latitude": 107.9624812
  },
  {
    "STT": 317,
    "Name": "\tPhòng khám Nha Khoa Hà Nội - Mc\t",
    "address": "\tSố 18 Hồng Vận, - P. Ka Long, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5315308,
    "Latitude": 107.9641702
  },
  {
    "STT": 318,
    "Name": "\tPhòng khám Chuyên Khoa Nội Ngoài Giờ\t",
    "address": "\tKhu 5, Phường Ka Long, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5333333,
    "Latitude": 107.9666667
  },
  {
    "STT": 319,
    "Name": "\tPhòng khám Chuyên Khoa Nội\t",
    "address": "\tSố 74B, Hùng Vương , Ka Long, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5311943,
    "Latitude": 107.9598569
  },
  {
    "STT": 320,
    "Name": "\tPhòng khám 5/8 Ka Long\t",
    "address": "\tSố nhà 23 phố 5/8 P. Ka Long, thành phố  Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5296664,
    "Latitude": 107.9622926
  },
  {
    "STT": 321,
    "Name": "\tPhòng khám Đa Khoa Y Cao Hà Nội Móng Cái\t",
    "address": "\tSố 34 Tuệ Tĩnh, P. Ka Long, thành phố  Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5288544,
    "Latitude": 107.963027
  },
  {
    "STT": 322,
    "Name": "\tPhòng khám Siêu Âm Ngọc Minh\t",
    "address": "\tSố nhà 88 phố Vườn Trầu, P. Trần Phú, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5307919,
    "Latitude": 107.9725175
  },
  {
    "STT": 323,
    "Name": "\tPhòng khám Răng Hàm Mặt Ngoài Giờ\t",
    "address": "\tSố nhà 51 đại lộ Hoà Bình, P. Trần Phú, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5276358,
    "Latitude": 107.9732508
  },
  {
    "STT": 324,
    "Name": "\tPhòng khám Chuyên Khoa Nội\t",
    "address": "\tSố 24 Trần Phú, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5308046,
    "Latitude": 107.968877
  },
  {
    "STT": 325,
    "Name": "\tPhòng khám Bệnh Chuyên Khoa Phụ Sản, Khhgđ\t",
    "address": "\tSố 58, Thương Mại, Trần Phú, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5326317,
    "Latitude": 107.9689148
  },
  {
    "STT": 326,
    "Name": "\tNha Khoa Hà Nội  Mc 2\t",
    "address": "\tSố 5, Trần Phú, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.530056,
    "Latitude": 107.968225
  },
  {
    "STT": 327,
    "Name": "\tPhòng khám Nội\t",
    "address": "\t105 Triều Dương, phường Trần Phú, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5299986,
    "Latitude": 107.9719691
  },
  {
    "STT": 328,
    "Name": "\tPhòng Siêu Âm Sản Tâm Đức\t",
    "address": "\tSố 5 ( tầng 1 ), Đông Trì, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5301805,
    "Latitude": 107.9689398
  },
  {
    "STT": 329,
    "Name": "\tPhòng khám Nội Khoa Tư Nhân Ngoài Giờ\t",
    "address": "\tThôn 7 xã Hải Đông, thành phố Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 330,
    "Name": "\tPhòng khám Nội\t",
    "address": "\tThôn 11, xã Hải Đông, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5394395,
    "Latitude": 107.8835642
  },
  {
    "STT": 331,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tThôn 9, xã Hải Xuân, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4984379,
    "Latitude": 107.9830272
  },
  {
    "STT": 332,
    "Name": "\tPhòng Răng Tân Tiến 2\t",
    "address": "\tThôn 9, xã Hải Xuân, thành phố Móng Cái, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.4984379,
    "Latitude": 107.9830272
  },
  {
    "STT": 333,
    "Name": "\tDịch Vụ Y Tế\t",
    "address": "\tThôn 8, Hải Tiến, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5370438,
    "Latitude": 107.8530457
  },
  {
    "STT": 334,
    "Name": "\tPhòng Chẩn Trị Y Học Cổ Truyền Hương Thông\t",
    "address": "\tKm12 xã Hải Tiến, Móng Cái, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5394738,
    "Latitude": 107.856929
  },
  {
    "STT": 335,
    "Name": "\tPhòng khám Sản Phụ Khoa 279 Hải Chi Siêu Âm Mầu 4 Chiều Ngoài Giờ \t",
    "address": "\tKhu 3, thị trấn Ba Chẽ, huyện Ba Chẽ, tỉnh Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.2731692,
    "Latitude": 107.285255
  },
  {
    "STT": 336,
    "Name": "\tPhòng khám Răng Hàm Mặt\t",
    "address": "\tKhu Bình Công 2, Thị trấn Bình Liêu,  Bình Liêu, Quảng Ninh\t",
    "StartTime": "\t8:00:00 AM\t",
    "EndTime": "\t17:00:00 PM\t",
    "Longtitude": 21.5258306,
    "Latitude": 107.3957643
  }
];
