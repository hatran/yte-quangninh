var datanhathuoc = [
 // {
 //   "Name": "Quầy thuốc Doanh Nghiệp Số 28 -Công Ty TNHH Thương Mại Thần Công Đại Việt",
 //   "address": "Tổ 4 khu Bắc Sơn, phường Cẩm Sơn, thành phố Cẩm Phả, Quảng Ninh",
 //   "Longtitude": 21.8437729,
 //   "Latitude": 106.2992912
 // },
 {
   "Name": "Nhà thuốc Bệnh Viện Đa Khoa Cẩm Phả",
   "address": "Bệnh viện đa khoa Cẩm Phả, đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0096076,
   "Latitude": 107.2768906
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 99 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Đìa Mối, xã An Sinh, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0958153,
   "Latitude": 106.6055534
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 20 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 2 thôn Đồng Tâm, xã Lê Lợi, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0169087,
   "Latitude": 107.0421324
 },
 {
   "Name": "Nhà thuốc Hằng Ngọc",
   "address": "Số 172 Tuệ Tĩnh, phường Thanh Sơn, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.0399603,
   "Latitude": 106.7521665
 },
 {
   "Name": "Nhà thuốc Bệnh Viện Đa Khoa Tỉnh Quảng Ninh",
   "address": "BVĐK tỉnh ,  phố Tuệ Tĩnh, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9539294,
   "Latitude": 107.0884569
 },
 {
   "Name": "Quầy thuốc Thúy Chinh 2 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Ki ốt số 2, chợ Cẩm Thạch, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0101852,
   "Latitude": 107.2564208
 },
 {
   "Name": "Nhà thuốc Hương Sơn",
   "address": "Số nhà 54A phố 2, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0598576,
   "Latitude": 106.5922003
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hòa Liên - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 6 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0334558,
   "Latitude": 106.9887685
 },
 {
   "Name": "Quầy thuốc Số 114 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Km 16, chợ Trung Tâm, phố Hoàng Hoa Thám, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.4498682,
   "Latitude": 107.7493787
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Kim Thùy - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 3, xã Hồng Thái Tây, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0512964,
   "Latitude": 106.6717207
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệpanh Đào- Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Thọ Tràng, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.048418,
   "Latitude": 106.61684
 },
 {
   "Name": "Nhà thuốc Hồng Đức",
   "address": "Số 497 tổ 75 khu 7, phường Cửa Ông, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Nhà thuốc Thắm Oanh",
   "address": "Số 169 Đường Tuệ Tĩnh, phường Ka Long, thành phố Móng Cái, Quảng Ninh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "Name": "Nhà thuốc Tình Quân",
   "address": "Số nhà 396 tổ 1 khu 1, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Nhà thuốc Thái Hà",
   "address": "Số nhà 179 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9563918,
   "Latitude": 107.0978244
 },
 {
   "Name": "Nhà thuốc Thanh Luyến",
   "address": "Số nhà 56 phố Vân Đồn khu 2, phường Trần Phú, thành phố Móng Cái, Quảng Ninh",
   "Longtitude": 21.5301855,
   "Latitude": 107.9702169
 },
 {
   "Name": "Quầy thuốc Dn Số 27 - Công Ty TNHHdp Hạ Long",
   "address": "Tổ 1 khu 6 phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.06284,
   "Latitude": 107.343399
 },
 {
   "Name": "Quầy thuốc Dn Số 28 - Công Ty TNHHdp Hạ Long",
   "address": "Thôn Yên Hòa, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0567778,
   "Latitude": 106.6202887
 },
 {
   "Name": "Nhà thuốc Tâm Đức 9",
   "address": "Tổ 43 khu 4, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9664599,
   "Latitude": 107.096792
 },
 {
   "Name": "Nhà thuốc Hương Lý",
   "address": "Số 265, tổ 5 khu 3, phường Yết Kiêu, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9600653,
   "Latitude": 107.0760905
 },
 {
   "Name": "Quầy thuốc Dn Số 60 - Công Ty TNHHdp Bạch Đằng",
   "address": "Ki ốt 159 Chợ Đầm Hà, thị trấn Đầm Hà, huyện Đầm Hà, Quảng Ninh",
   "Longtitude": 21.3500114,
   "Latitude": 107.5992193
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Khánh Hòa - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Trại Thông, xã Bình Khê, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.104795,
   "Latitude": 106.585353
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Số 65 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 06 đường Trần Hưng Đạo, phường Trần Hưng Đạo, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9562358,
   "Latitude": 107.0851374
 },
 {
   "Name": "Quầy thuốc Số 115 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt chợ Bạch Đằng, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0059919,
   "Latitude": 107.2479638
 },
 {
   "Name": "Nhà thuốc Minh Hải",
   "address": "Tổ 2 khu 2, phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.055278,
   "Latitude": 107.343056
 },
 {
   "Name": "Nhà thuốc An Phương",
   "address": "Số 63, tổ 86 khu 8, phường Cửa Ông, thành phố cẩm Phả, Quảng Ninh",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Nhà thuốc Hạnh Loan",
   "address": "Số 556 Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9506193,
   "Latitude": 107.1325327
 },
 {
   "Name": "Nhà thuốc Thu Hằng",
   "address": "Số nhà 117 tổ 7, phường Cẩm Bình, thành phố Cẩm Phả, quảng Ninh",
   "Longtitude": 21.0002112,
   "Latitude": 107.2873282
 },
 {
   "Name": "Nhà thuốc Lâm Đoan",
   "address": "Số 365 đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0118793,
   "Latitude": 107.2790011
 },
 {
   "Name": "Nhà thuốc Hải Hòa",
   "address": "Khu 4, phường Hải Hòa, thành phố Móng Cái, Quảng Ninh",
   "Longtitude": 21.532994,
   "Latitude": 107.9674392
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Mai Hiếu - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 79 khu Vĩnh Thông, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0598621,
   "Latitude": 106.5869951
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Cường Mai - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 8, xã Hải Đông, thành phố Móng Cái, Quảng Ninh",
   "Longtitude": 21.5394395,
   "Latitude": 107.8835642
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thanh Huyền - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 190 Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.4513441,
   "Latitude": 107.7561851
 },
 {
   "Name": "Quầy thuốc Số 15 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Km 7 khu 1 phường Hải Yên, thành phố Móng Cái, Quảng Ninh",
   "Longtitude": 21.5239006,
   "Latitude": 107.9167514
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Bích Liên - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Ki ốt số 19 chợ Phương Đông, phường Phương Đông, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.039657,
   "Latitude": 106.7248828
 },
 {
   "Name": "Nhà thuốc Số 2 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Tổ 13 khu 3, phường Hà Phong, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.976522,
   "Latitude": 107.1543601
 },
 {
   "Name": "Nhà thuốc Minh Anh",
   "address": "Số 20, tổ 1 khu 7, phường Bãi Cháy, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9643837,
   "Latitude": 107.0480378
 },
 {
   "Name": "Nhà thuốc Tâm Đức Iii",
   "address": "Tổ 1 khu 5, phường Mông Dương, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0822391,
   "Latitude": 107.3021069
 },
 {
   "Name": "Nhà thuốc Tâm An",
   "address": "SN 248 Đường Trần Phú, phường cao Xanh, Tphường Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9760021,
   "Latitude": 107.0854929
 },
 {
   "Name": "Nhà thuốc Thành Trung",
   "address": "Số 108 Tổ 20 khu 6, phường Quang Trung, Tphường Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.1057847,
   "Latitude": 106.8075136
 },
 {
   "Name": "Nhà thuốc Thuận Hưng - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 153 tổ 2 khu 4, phường Giếng Đáy, Tphường Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Hồng Linh - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 195, tổ 7 khu 3, phường Hồng Hà, Tphường Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9417785,
   "Latitude": 107.1277751
 },
 {
   "Name": "Nhà thuốc Viên Hồng",
   "address": "Số nhà 785 ,  Đường Trần Phú, phường Cẩm Thủy, Tphường cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0098992,
   "Latitude": 107.2656074
 },
 {
   "Name": "Nhà thuốc Cường Long",
   "address": "Số 74 Kênh Liêm, phường Cao Thắng, Tphường Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9581057,
   "Latitude": 107.0921765
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hoàng Hiếu - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Vĩnh Thái, xã Hồng Thái Đông, thị xã Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0518672,
   "Latitude": 106.683662
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 82 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Xóm 1, thôn 1, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9387415,
   "Latitude": 106.796489
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 9- Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Tổ 2 khu 10, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.02332,
   "Latitude": 106.988
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hạnh Liên - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 172, phường Vàng Danh, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.1102653,
   "Latitude": 106.8060388
 },
 {
   "Name": "Quầy thuốc Số 62 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 62 Đường Triều Dương, phường Trần Phú, Tphường Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5313545,
   "Latitude": 107.970876
 },
 {
   "Name": "Nhà thuốc Dn Hải Tiến- Công Ty TNHHdp Hạ Long",
   "address": "Tổ 84 khu 8 phường Hà Khẩu, thành phố Hạ Long",
   "Longtitude": 20.9819628,
   "Latitude": 106.9889904
 },
 {
   "Name": "Nhà thuốc Minh Anh 2",
   "address": "Số 173, tổ 1 khu 4, phường Giếng Đáy, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Số 10 Hùng Linh - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 24, tổ 5 khu 2 Anh Đào, phường Bãi Cháy, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9579074,
   "Latitude": 107.0226806
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 58 Dũng Thương - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Xã Quảng La, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0899707,
   "Latitude": 106.8849557
 },
 {
   "Name": "Quầy thuốc Tâm Tâm - Công Ty TNHH Dược Phẩm Hải Bình",
   "address": "SN 519, tổ 2 khu 4, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh",
   "Longtitude": 21.053875,
   "Latitude": 107.4351532
 },
 {
   "Name": "Cơ Sở thuốc Đông Y Gia Truyền Cụ Lang Lợi",
   "address": "Số nhà 528, tổ 9 khu 6, phường Giếng Đáy, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Cơ Sở Bán Lẻ thuốc Yhct",
   "address": "Ki ốt số 21 + 22 chợ Trung Tâm Cẩm Phả ,  phường Cẩm Trung, Tphường Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0092042,
   "Latitude": 107.2730462
 },
 {
   "Name": "Nhà thuốc Minh Hà",
   "address": "Số 344 đường Minh Hà, tổ 3 khu 1, phường Hà Tu, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Nhà thuốc Đông Bắc - Công Ty TNHH Thương Mại Thần Công Đại Việt",
   "address": "Số 751 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9463274,
   "Latitude": 107.1087437
 },
 {
   "Name": "Nhà thuốc Hùng Hằng - Công Ty TNHH Thương Mại Thần Công Đại Việt",
   "address": "Tổ 5a Khu 4a, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9722852,
   "Latitude": 107.0834734
 },
 {
   "Name": "Nhà thuốc Diệp Ngọc",
   "address": "Tổ 30 khu 3, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.96077,
   "Latitude": 107.0942
 },
 {
   "Name": "Nhà thuốc Trung Hiếu",
   "address": "Số 134 Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9413546,
   "Latitude": 107.1181626
 },
 {
   "Name": "Nhà thuốc Y Cao Hà Nội",
   "address": "Số 857 Đường Trần Phú, phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0101542,
   "Latitude": 107.2539561
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 158 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Ki ốt 127 chợ Cái Rồng, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh",
   "Longtitude": 21.0681423,
   "Latitude": 107.4236013
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Linh Huệ - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Làng Đài, xã Đông Hải, huyện Tiên Yên, Quảng Ninh",
   "Longtitude": 21.333565,
   "Latitude": 107.5201909
 },
 {
   "Name": "Nhà thuốc Minh Tâm",
   "address": "Tổ 1 khu 1, phường Giếng Đáy, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hùng Yến - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 30, Tổ 3 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 20.9958136,
   "Latitude": 106.9705427
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Bích Liên 2 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 01 phố Đông Tiến, thị trấn Tiên Yên, huyện Tiên Yên, Quảng Ninh",
   "Longtitude": 21.3337741,
   "Latitude": 107.4091219
 },
 {
   "Name": "Nhà thuốc Xuân Thủy",
   "address": "Tổ 66 khu 8, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.966594,
   "Latitude": 107.096763
 },
 {
   "Name": "Quầy thuốc Số 26 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Số 15 khu 5 tầng Phố Mới, phường Trần Hưng Đạo, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9562721,
   "Latitude": 107.0849499
 },
 {
   "Name": "Cơ Sở Bán Lẻ thuốc Y Học Cổ Truyền",
   "address": "Số nhà 48, tổ 7 khu 4A, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0020567,
   "Latitude": 107.269595
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 36 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Khu 1, phường Phong Hải, thị Xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.9372727,
   "Latitude": 106.8294239
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Vũ Loan - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 4 ,  phố Chu Văn An, thị trấn Đầm hà, huyện Đầm hà, Quảng Ninh",
   "Longtitude": 21.3504791,
   "Latitude": 107.6019029
 },
 {
   "Name": "Nhà thuốc Trinh Hương",
   "address": "Số nhà 17, đườngTrần Phú, phường Cẩm Tây, Tphường Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0129841,
   "Latitude": 107.2824314
 },
 {
   "Name": "Quầy thuốc Dn Tùng Dung - Công Ty TNHHdp Hạ Long",
   "address": "Thôn 10A, xã Hải Xuân, Tphường Móng Cái, Quảng Ninh",
   "Longtitude": 21.4982894,
   "Latitude": 107.9828138
 },
 {
   "Name": "Quầy thuốc Bệnh Viện Vân Đồn- Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "BVĐK Vân Đồn,  Thôn 12, xã Hạ Long, huyện Vân Đồn, Quảng Ninh",
   "Longtitude": 20.8758105,
   "Latitude": 107.4944308
 },
 {
   "Name": "Quầy thuốc Ngọc Bích - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "xã Liên Vị, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.8625707,
   "Latitude": 106.8001396
 },
 {
   "Name": "Quầy thuốc Hoa Phú - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Lục Nà, xã Lục Hồn, huyện Bình Liêu, Quảng Ninh",
   "Longtitude": 21.5523751,
   "Latitude": 107.4270206
 },
 {
   "Name": "Quầy thuốc Hùng Thảo - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Số 450, tổ 2 khu 7, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0256819,
   "Latitude": 106.9983133
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Bích Thủy - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 120, tổ 22 khu 7, phường Vàng Danh, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.1102653,
   "Latitude": 106.8060388
 },
 {
   "Name": "Nhà thuốc Yến Thanh",
   "address": "Số nhà 31, Tổ 2 Khu phố Hòa Bình, phường Cẩm Tây, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.015282,
   "Latitude": 107.2851737
 },
 {
   "Name": "Quầy thuốc Tâm Tâm - Công Ty TNHH Thương Mại Thần Công Đại Việt",
   "address": "Số nhà 519, tổ 2 khu 4, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh",
   "Longtitude": 21.053875,
   "Latitude": 107.4351532
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Công Dũng - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Cẩm Thành, xã Cẩm La, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9048714,
   "Latitude": 106.8104256
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Ninh Giang - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Ki ốt Chợ km 9 Quang Hanh, Tphường Cẩm Phả, Quảnh Ninh",
   "Longtitude": 21.0082107,
   "Latitude": 107.2271147
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Chiến Nhung - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Ki ốt Chợ km 9 Quang Hanh, Tphường Cẩm Phả, Quảnh Ninh",
   "Longtitude": 21.0082107,
   "Latitude": 107.2271147
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Minh Tiến - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Tràng Bảng, xã Tràng An, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0968826,
   "Latitude": 106.572189
 },
 {
   "Name": "Quầy thuốc Dn Số 62 - Công Ty TNHHdp Bạch Đằng",
   "address": "Thôn Tân Yên, xã Hồng Thái Đông, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0591112,
   "Latitude": 106.6918802
 },
 {
   "Name": "Nhà thuốc Ngọc Hà",
   "address": "A7 nhà A, chung cư cột 8, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.937282,
   "Latitude": 107.1256712
 },
 {
   "Name": "Nhà thuốc Hoa Lệ",
   "address": "Số nhà 57, tổ 2 khu 4, phường Hà Lầm, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9579074,
   "Latitude": 107.0226806
 },

 {
   "Name": "Nhà thuốc Số 9",
   "address": "Số nhà 87 Kênh Liêm, phường Bạch Đằng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9582493,
   "Latitude": 107.0917291
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thuận Tâm - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Tổ 6 khu Công Nông, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Quầy thuốc Số 6 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc",
   "address": "Ki ốt chợ Hoành Bồ khu 10, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0235233,
   "Latitude": 106.9880336
 },
 {
   "Name": "Quầy thuốc Số 121 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Khu 5, phường Hải Hòa, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.517982,
   "Latitude": 108.0218071
 },
 {
   "Name": "Quầy thuốc Số 52 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số nhà 168, đường Trần Phú, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.012772,
   "Latitude": 107.283515
 },
 {
   "Name": "Quầy thuốc Số 09 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 113, tổ 1 khu 1, phường Hà Trung, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Quầy thuốc Số 33 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt số 122 chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9479461,
   "Latitude": 107.1295067
 },
 {
   "Name": "Cơ Sở Kinh Doanh thuốc Đông Y Thanh Thảo",
   "address": "Ki ốt số 224 chợ Trung Tâm, thị trấn  Mạo Khê, Huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Cơ Sở Bán Lẻ thuốc Đông Y",
   "address": "Tổ 60 khu 7, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.96472,
   "Latitude": 107.0978
 },
 {
   "Name": "Quầy thuốc Hà Anh - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Chợ Mới Kim Sen, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0719973,
   "Latitude": 106.5686164
 },
 {
   "Name": "Quầy thuốc Sơn Phú - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Nhuệ Hổ, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.070062,
   "Latitude": 106.5553799
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hoàng Trang - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 1, xã Quảng Phong, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.4290946,
   "Latitude": 107.7155099
 },
 {
   "Name": "Nhà thuốc Tuấn Hòa",
   "address": "Số 192 đường Quang Trung, phường Quang Trung, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.0340907,
   "Latitude": 106.7756767
 },
 {
   "Name": "Quầy thuốc Số 7 Nguyễn Toàn - Công Ty TNHH Dược Phẩm Tm Thái Ngọc",
   "address": "Số 98 Vĩnh Thông, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.056883,
   "Latitude": 106.598092
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Vũ Hà- Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 1 khu Vĩnh Tuy 1, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Tâm Ba - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "SN 48 khu phố 2, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Dsthuyện  Nguyễn Thị Quyết",
   "address": "Thôn Cẩm Thành, xã Cẩm La, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9048714,
   "Latitude": 106.8104256
 },
 {
   "Name": "Nhà thuốc Mạnh Thanh - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Khu Đường Ngang, phường Minh Thành, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.00493,
   "Latitude": 106.8552697
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 7 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Tổ 6 khu Vĩnh Phú, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 32 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 8, tổ 2 khu 10, thị trấn Trới, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0256819,
   "Latitude": 106.9983133
 },
 {
   "Name": "Nhà thuốc Linh Chi",
   "address": "Tổ 5 khu 5B, phường Bãi Cháy, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9606657,
   "Latitude": 107.0613154
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 10 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Thôn 4, xã Hoàng Tân, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.9135556,
   "Latitude": 106.9240554
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thanh Thùy - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 3 khu Vĩnh Hồng, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 140 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 6, xã Đức Chính, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0890514,
   "Latitude": 106.5192566
 },
 {
   "Name": "Quầy thuốc Số 136 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Số 140, tổ 8 khu1, phường Hà Tu, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9546746,
   "Latitude": 107.1425442
 },
 {
   "Name": "Nhà thuốc Kết Nam",
   "address": "Số nhà 859, tổ 6 khu 10, phường  Hồng Hải,Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9499748,
   "Latitude": 107.1041465
 },
 {
   "Name": "Nhà thuốc Quang Minh",
   "address": "SN 328, tổ 3 khu2, phường Yết Kiêu, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9600653,
   "Latitude": 107.0760905
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 26 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Tổ 11 khu 4, phường  Hùng Thắng, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9576774,
   "Latitude": 107.0007987
 },
 {
   "Name": "Quầy thuốc Minh Hằng - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "SN 19 tổ 1, khu Vĩnh Lập,thị trấn  Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0598621,
   "Latitude": 106.5869951
 },
 {
   "Name": "Quầy thuốc Ngọc Liên - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Số 82, Hoàng Hoa Thám, thị trấn Mạo Khê, huyện  Đông Triều, Quảng Ninh",
   "Longtitude": 21.0569546,
   "Latitude": 106.6040799
 },
 {
   "Name": "Quầy thuốc Sạc Chi - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Tổ 1 khu Vĩnh Quang, thị trấn Mạo Khê, huyện  Đông Triều, Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Số 29 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Đông Ngũ, xã Đông Ngũ, huyện Tiên Yên, Quảng Ninh",
   "Longtitude": 21.337168,
   "Latitude": 107.4618143
 },
 {
   "Name": "Quầy thuốc Thành Thủy - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn 5, xã Quảng Long, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.4612614,
   "Latitude": 107.6951905
 },
 {
   "Name": "Quầy thuốc Thảo Hiền - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Ki ốt chợ Trới, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0281241,
   "Latitude": 106.9889904
 },
 {
   "Name": "Quầy thuốc Thành Hương - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn 1, xóm 1, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9387415,
   "Latitude": 106.796489
 },
 {
   "Name": "Công Ty Trách Nhiệm Hữu Hạn Dược Phúc Việt",
   "address": "Số 216 đường Bãi Muối, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9687487,
   "Latitude": 107.0970687
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Phương Thùy- Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 103 khu 2, phường Đông Triều, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0836091,
   "Latitude": 106.514091
 },
 {
   "Name": "Quầy thuốc Dn Số 31 - Công Ty TNHHdp Bạch Đằng",
   "address": "Km 11, Phường Minh Thành, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 21.00493,
   "Latitude": 106.8552697
 },
 {
   "Name": "Nhà thuốc Số 6 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt chợ Trung Tâm Cẩm Phả, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0079046,
   "Latitude": 107.2705186
 },
 {
   "Name": "Công Ty TNHH Thương Mại Thàn Công Đại Việt",
   "address": "Số 58 tổ 3 khu 6, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9590927,
   "Latitude": 107.0728813
 },
 {
   "Name": "Nhà thuốc An An - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Tổ 66 khu 8, đường Bãi Muối, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9687487,
   "Latitude": 107.0970687
 },
 {
   "Name": "Nhà thuốc Thúy Hằng 2",
   "address": "SN 02 tổ 1 Minh Khai, phường Đại Yên, Tphường Hạ Long, Quảng Ninhuyện ",
   "Longtitude": 20.9649885,
   "Latitude": 106.9432389
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Số 12 Minh Đức - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 560, tổ 57 khu 6, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9590927,
   "Latitude": 107.0728813
 },
 {
   "Name": "Nhà thuốc Ba Chẽ",
   "address": "Khu 2, thị trấn Ba Chẽ, huyện Ba Chẽ, tỉnh Quảng Ninh",
   "Longtitude": 21.2718654,
   "Latitude": 107.2836989
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Phượng Hồng - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Thôn 5, xã Sông Khoai, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9768581,
   "Latitude": 106.8119382
 },
 {
   "Name": "Quầy thuốc Số 32 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Tổ 7 khu 2, phường Giếng Đáy, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9815961,
   "Latitude": 107.0210624
 },
 {
   "Name": "Nhà thuốc 262 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 262 ,  Đường Cao Xanh, phường Cao Xanh, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9744635,
   "Latitude": 107.081176
 },
 {
   "Name": "Quầy thuốc Số 03 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Bệnh viện đa khoa khu vực Móng Cái, TP Móng Cái, Quảng Ninh",
   "Longtitude": 21.5249181,
   "Latitude": 107.9610462
 },
 {
   "Name": "Quầy thuốc Quang Minh - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Số 88 Chợ Cột, thị trấn Đông Triều, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0836378,
   "Latitude": 106.5161416
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Phương Thảo - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Gia Mô, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0707941,
   "Latitude": 106.5642407
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Phương Thảo - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Gia Mô, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0707941,
   "Latitude": 106.5642407
 },
 {
   "Name": "Quầy thuốc Đức Chung - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "SN 665,  Đường Nguyễn Đức Cảnh, phường Quang Hanh, Tphường  Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0072997,
   "Latitude": 107.2255782
 },
 {
   "Name": "Nhà thuốc Bảo Chinh",
   "address": "Số 103 B,  Hùng Vương, phường Ka Long, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5271014,
   "Latitude": 107.9711471
 },
 {
   "Name": "Nhà thuốc Thanh Chương",
   "address": "Số nhà 22, phố Trần Bình Trọng, thị trấn Quảng Hà , huyện Hải Hà, tỉnh Quảng Ninh",
   "Longtitude": 21.452286,
   "Latitude": 107.7521534
 },
 {
   "Name": "Quầy thuốc Số 22 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 330,  Đường Hà Lầm, phường Hà Trung, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9585894,
   "Latitude": 107.1223572
 },
 {
   "Name": "Quầy thuốc Số 12 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 9, đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5353709,
   "Latitude": 107.9724429
 },
 {
   "Name": "Quầy thuốc Số 18 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 108B, đường Hùng Vương, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5278509,
   "Latitude": 107.9703632
 },
 {
   "Name": "Quầy thuốc Số 44 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt số 15, chợ Suối Khoáng, phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 20.9846214,
   "Latitude": 107.1953201
 },
 {
   "Name": "Quầy thuốc Số 55 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số nhà 86, tổ 18 A khu 6, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.1057847,
   "Latitude": 106.8075136
 },
 {
   "Name": "Quầy thuốc Số 27 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 53 phố Anh Đào, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9596227,
   "Latitude": 107.050913
 },
 {
   "Name": "Nhà thuốc Dn Dương Dịu - Công Ty Cptm Và Dp Nam Việt",
   "address": "Số nhà 904 tổ 109 khu 10B, phường Cửa Ông, Tphường Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Quầy thuốc Số 82 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt số 12 chợ Cẩm Đông, phường cẩm Đông, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Quầy thuốc Số 85 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 369 Đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0118793,
   "Latitude": 107.2790011
 },
 {
   "Name": "Quầy thuốc Số 80 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 383 tổ 10G, phường Cẩm Thịnh, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 20.9914726,
   "Latitude": 107.3434916
 },
 {
   "Name": "Nhà thuốc Số 4 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 264 Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9411638,
   "Latitude": 107.1214625
 },
 {
   "Name": "Nhà thuốc Phạm Thị Hồng Oanh",
   "address": "Số nhà 105, đường Cao Thắng, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9628713,
   "Latitude": 107.0962345
 },
 {
   "Name": "Quầy thuốc Số 48 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Chợ Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0347844,
   "Latitude": 106.769557
 },
 {
   "Name": "Nhà thuốc Tiên Yên",
   "address": "Số nhà 228 ,  Phố Thống Nhất, Thị trấn Tiên Yên, huyện Tiên yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3289859,
   "Latitude": 107.3966896
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thanh Hà - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 131 đường Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Số 42 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 26, tổ 4 khu 9, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9643837,
   "Latitude": 107.0480378
 },
 {
   "Name": "Quầy thuốc Số 30 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 437, đường Hạ Long, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9547931,
   "Latitude": 106.9712112
 },
 {
   "Name": "Quầy thuốc Số 58 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 1044 ,  đường Trần Phú, phường Cẩm Thạch, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0101542,
   "Latitude": 107.2539561
 },
 {
   "Name": "Quầy thuốc Số 41 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 194 đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0118793,
   "Latitude": 107.2790011
 },
 {
   "Name": "Nhà thuốc Thảo Linh - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 1 khu 4, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9390906,
   "Latitude": 107.125438
 },
 {
   "Name": "Quầy thuốc Số 10 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Khu 8, phường Hải Hòa, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5352465,
   "Latitude": 108.0124469
 },
 {
   "Name": "Cơ Sở Kinh Doanh thuốc Yhct Kim Ngân - Cơ Sở 2",
   "address": "Ki ốt số 8, chợ trung Tâm Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "Số 58, tổ 2B khu 1, phường Hồng Hà, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hạnh Tùng - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Trung tâm thương mại khu đô thị mới, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.066915,
   "Latitude": 106.5694561
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Kiên Tuyền - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 5 Hoàng Hoa Thám, thị trấn Mạo Khê, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Mai Hoa - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 4, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0898979,
   "Latitude": 106.5201084
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Bảo Châu - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Khê Hạ, xã Việt Dân, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0958153,
   "Latitude": 106.6055534
 },
 {
   "Name": "Nhà thuốc Huy Hiếu",
   "address": "Số 36 phố Hải Phong, phường Hồng Hải, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9417784,
   "Latitude": 107.1096306
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 9 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Chợ cái rồng, khu 8, thị trấn Cái Rồng, huyện Vân Đồn, Quảng Ninh",
   "Longtitude": 21.065121,
   "Latitude": 107.424283
 },
 {
   "Name": "Quầy thuốc Dn Số 48 - Công Ty TNHHdp Bạch Đằng",
   "address": "Chợ Km 11, Phường Minh Thành, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 21.00493,
   "Latitude": 106.8552697
 },
 {
   "Name": "Nhà thuốc Hưng Thịnh",
   "address": "Số 74, tổ 2 khu 1, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Nhà thuốc Hồng Thân",
   "address": "Ki ốt số 3 chợ Hà Lầm, phường Hà Lầm, Tphường Hạ Long, QuảngNinh",
   "Longtitude": 20.9666167,
   "Latitude": 107.108266
 },
 {
   "Name": "Nhà thuốc Số 16",
   "address": "Số nhà 16, tổ 3 khu 1, phường Cao Xanh, Tphường Hạ Long, Q. Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Số 1, tổ 1 khu 1, phường Trần Hưng Đạo, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Nhà thuốc Mai Huyền - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 130 Đường Cao Xanh, phường Cao Xanh, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9744635,
   "Latitude": 107.081176
 },
 {
   "Name": "Quầy thuốc Số 52- Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 153, tổ 1 khu Nam Trung, phường Nam Khê, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 21.0166472,
   "Latitude": 106.8231871
 },
 {
   "Name": "Quầy thuốc Số 31- Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt số 6, chợ Trung Tâm, Tphường  Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0347844,
   "Latitude": 106.769557
 },
 {
   "Name": "Cơ Sở Bán thuốc Đông Y, thuốc Từ Dược Liệu",
   "address": "Thôn 5, xã Quảng Chính, huyện Hải Hà, tỉnh Quảng Ninh",
   "Longtitude": 21.4563555,
   "Latitude": 107.7060389
 },
 {
   "Name": "Cơ Sở thuốc Đông Y Thiên Thảo Đường",
   "address": "Tổ 30D khu 2, phường Cao Xanh, Tphường Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9722852,
   "Latitude": 107.0834734
 },
 {
   "Name": "Nhà thuốc Ngọc Hoa",
   "address": "Ki ốt số 10 chợ Cẩm Thành, phường Cẩm Thành, TP Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0125041,
   "Latitude": 107.279108
 },
 {
   "Name": "Nhà thuốc Hà Thu",
   "address": "Số nhà 237, đường Cao Thắng, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9608936,
   "Latitude": 107.0925003
 },
 {
   "Name": "Quầy thuốc Số 5 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Ki ốt chợ Sa Tô, phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9777619,
   "Latitude": 107.0872968
 },
 {
   "Name": "Quầy thuốc Số 14 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 6 khu 8, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9499748,
   "Latitude": 107.1041465
 },
 {
   "Name": "Quầy thuốc Số 8 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 12 ,  đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5353709,
   "Latitude": 107.9724429
 },
 {
   "Name": "Quầy thuốc Số 35 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 711 ,  đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.986098900000002,
   "Latitude": 106.9962519
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 46 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Thôn 1, xã Hạ Long, huyệnVân Đồn, Quảng Ninh",
   "Longtitude": 21.0981538,
   "Latitude": 107.4553657
 },
 {
   "Name": "thuốc Đông Y Đại Lợi",
   "address": "Số nhà 90, tổ 15 khu 1, phường Bạch Đằng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 8 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Xã Nguyễn Huệ, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0958153,
   "Latitude": 106.6055534
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 9 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Thôn Mễ Xá 1, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0661883,
   "Latitude": 106.5260001
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 22 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Vườn Cau, xã Sơn Dương, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.067638,
   "Latitude": 106.9764447
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 103 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.064797,
   "Latitude": 106.5268278
 },
 {
   "Name": "Quầy thuốc Khe Sím - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Khe Sím, xã Dương Huy, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0216205,
   "Latitude": 107.2731686
 },
 {
   "Name": "Nhà thuốc Tâm Phúc",
   "address": "Số 17, tổ 20 khu 3, phường Trưng Vương, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.098618,
   "Latitude": 106.7927658
 },
 {
   "Name": "Nhà thuốc Liên Long - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số nhà 9, tổ 6 khu 2, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9579074,
   "Latitude": 107.0226806
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Số 11- Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 32, tổ 2 khu 2, phường Hồng Hải, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9579074,
   "Latitude": 107.0226806
 },
 {
   "Name": "Nhà thuốc Ds Phạm Thị Luồng",
   "address": "Số 59 Nguyễn Du, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0347397,
   "Latitude": 106.7703621
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp 115 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Đông Hợp, xã Đông Xá, huyện Vân Đồn, tỉnh Quảng Ninh",
   "Longtitude": 21.0699039,
   "Latitude": 107.4138469
 },
 {
   "Name": "Nhà thuốc Thiện Phúc",
   "address": "Số 293, tổ 31C khu 3, phường Cao Xanh, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9722852,
   "Latitude": 107.0834734
 },
 {
   "Name": "Nhà thuốc 66",
   "address": "Số nhà 493, tổ 5 khu 4, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.970539,
   "Latitude": 107.1144837
 },
 {
   "Name": "Nhà thuốc Số 2 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Số 289 ,  Đường Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9557688,
   "Latitude": 107.0980769
 },
 {
   "Name": "Nhà thuốc Hợi Liên",
   "address": "Tổ 39 khu 4, phường Hà Trung, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9648457,
   "Latitude": 107.1277751
 },
 {
   "Name": "Quầy thuốc Số 81 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Ki ốt chợ Cẩm Đông, phường cẩm Đông, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Quầy thuốc Số 110 - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 4, phố Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.452286,
   "Latitude": 107.7521534
 },
 {
   "Name": "Nhà thuốc Số 2 - Bệnh Viện Đa Khoa Tỉnh Quảng Ninh",
   "address": "Tầng 1 nhà D khu khám bệnh theo yêu cầu BVĐK tỉnh ,  phường Bạch Đằng, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9519114,
   "Latitude": 107.0819592
 },
 {
   "Name": "Nhà thuốc Bảo Tín",
   "address": "Số 115,  Đường Tuệ Tĩnh, phường  Thanh Sơn, Tphường  Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0374998,
   "Latitude": 106.7516414
 },
 {
   "Name": "Nhà thuốc Quốc Sang",
   "address": "Số 131,  Đường Trần Nhân Tông, phường  Yên Thanh, Tphường  Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0344942,
   "Latitude": 106.7517934
 },
 {
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Số 167, tổ 4 khu Tây Sơn 1, phường  Cẩm Sơn, Tphường  Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0092731,
   "Latitude": 107.3109745
 },
 {
   "Name": "Nhà thuốc Vinh Huệ",
   "address": "Tổ 4, khu Diêm Thủy, phường Cẩm Bình, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0002112,
   "Latitude": 107.2873282
 },
 {
   "Name": "Nhà thuốc Khánh Ngọc",
   "address": "Số nhà 225, T8K6, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Phương Vy - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 1, khu Vĩnh Tuy 1, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Đức Luân - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Kim Sen, xã Kim Sơn, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0778256,
   "Latitude": 106.5687195
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hương Lộc- Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Làng, xã Thống Nhất, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0313655,
   "Latitude": 107.1024181
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Quỳnh Hoa - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn An Biên 1, xã Lê Lợi, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0156018,
   "Latitude": 107.0063023
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 22 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Thôn Chợ, xã Thống Nhất, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0351254,
   "Latitude": 107.0893798
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 6 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "SN 162,  Đường Hoàng Hoa Thám, thị trấn  Mạo Khê, huyện  Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 42 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 53 ,  Đường Nguyễn Trãi, thị trấn Trới, huyện Hoành Bồ, tỉnh Quảng Ninh",
   "Longtitude": 21.0375125,
   "Latitude": 106.9937795
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 51 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Khu 3, phường Hà An, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9241324,
   "Latitude": 106.8583328
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 40 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Khu 5, phường Phong Cốc, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.8776718,
   "Latitude": 106.7647475
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 52 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Khu Lâm Sinh II, phường Minh Thành, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.0073587,
   "Latitude": 106.8558034
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 55 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Thôn 8, xã Hạ Long, huyện Vân Đồn, tỉnh Quảng Ninh",
   "Longtitude": 21.0623461,
   "Latitude": 107.4913489
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 5 - Công Ty Cổ Phần Dược Phẩm Thành Đức",
   "address": "SN 210,  Đường Nguyễn Đức Cảnh, phường  Quang Hanh, Tphường  Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0072997,
   "Latitude": 107.2255782
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 2 Công Dung - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc",
   "address": "Xuân Viên 3, xã Xuân Sơn, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0777755,
   "Latitude": 106.5488153
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 3 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc",
   "address": "Thôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0475916,
   "Latitude": 106.6818326
 },
 {
   "Name": "Quầy thuốc Số 52 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 1, khu Nam Trung, phường Nam Khê, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0232141,
   "Latitude": 106.8121334
 },
 {
   "Name": "Cơ Sở Kinh Doanh thuốc Đông Y, thuốc Từ Dược Liệu",
   "address": "Khu 2, phường Phong Hải, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9089731,
   "Latitude": 106.8266873
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Chung Hiên - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Thượng Thông, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0475916,
   "Latitude": 106.6818326
 },
 {
   "Name": "Quầy thuốc Số 79 - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "SN 150,  Hoàng Hoa Thám, thị trấn  Mạo Khê, huyện  Đông Triều, Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Nhà thuốc Bệnh Viện Bãi Cháy",
   "address": "Bệnh viện Bãi Cháy, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9761578,
   "Latitude": 107.0143062
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Tùng Khang - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Đồng Mô, xã Hoành Mô, huyện Bình Liêu, Quảng Ninh",
   "Longtitude": 21.5963466,
   "Latitude": 107.4883052
 },
 {
   "Name": "Hộ Kinh Doanh thuốc Y Học Cổ Truyền Trường Mai",
   "address": "Tổ 3 khu 5A, phường Cẩm Trung, Tphường Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0099574,
   "Latitude": 107.2749255
 },
 {
   "Name": "Quầy thuốc Số 13 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Khu 7, Km 3, phường Hải Yên, Tphường Móng Cái, Quảng Ninh",
   "Longtitude": 21.5285555,
   "Latitude": 107.9319352
 },
 {
   "Name": "Quầy thuốc Số 25 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 7 khu 1, phường Mông Dương, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.055278,
   "Latitude": 107.343056
 },
 {
   "Name": "Quầy thuốc Số 66 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 292 đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.986098900000002,
   "Latitude": 106.9962519
 },
 {
   "Name": "Quầy thuốc Số 17 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 54, khu 5, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9722997,
   "Latitude": 106.9998899
 },
 {
   "Name": "Quầy thuốc Số 19 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 245 ,  đường An Tiêm, phường Hà Khẩu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.986098900000002,
   "Latitude": 106.9962519
 },
 {
   "Name": "Quầy thuốc Số 20 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 234 ,  đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9585581,
   "Latitude": 107.1526733
 },
 {
   "Name": "Quầy thuốc Số 113 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 841 ,  đường Trần Phú, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.010141,
   "Latitude": 107.2589275
 },
 {
   "Name": "Quầy thuốc Số 37 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 106 ,  đường Lý Bôn, phường Cẩm Đông, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Nhà thuốc Doanh Nghiệp Hải Linh - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Tổ 7 khu Tre Mai, phường Nam Khê, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.0265928,
   "Latitude": 106.8065339
 },
 {
   "Name": "Quầy thuốc Dn Mai Hiên - Công Ty TNHHdp Hạ Long",
   "address": "Số 132 tổ 1 khu Công Nông, phường Mạo Khê, TX. Đông Triều, Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Ngọc Lan - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Đội 2, thôn Kim Sen, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0719973,
   "Latitude": 106.5686164
 },
 {
   "Name": "Nhà thuốc Chu Kim Chung",
   "address": "Số nhà 603,  Đường Nguyễn văn Cừ, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9452755,
   "Latitude": 107.1243771
 },
 {
   "Name": "Nhà thuốc Ngân Sơn",
   "address": "Số nhà 87, tổ 2 khu 5, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hương Lý- Công Ty TNHH Dp Hạ Long",
   "address": "Số 16 phố Thống Nhất, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3289859,
   "Latitude": 107.3966896
 },
 {
   "Name": "Nhà thuốc Trọng Vinh",
   "address": "Tổ 9 khu 2 phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9593434,
   "Latitude": 107.0227511
 },
 {
   "Name": "Quầy thuốc Hải Thủy- Công Ty TNHH Dp Hồng Dương",
   "address": "Số 118 ,  Nguyễn Bình, thị trấn Đông Triều, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.087021,
   "Latitude": 106.516197
 },
 {
   "Name": "Quầy thuốc Khánh Ngọc- Công Ty TNHH Dp Hồng Dương",
   "address": "Thôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.064797,
   "Latitude": 106.5268278
 },
 {
   "Name": "Quầy thuốc Huyền Trang- Công Ty TNHH Dp Hồng Dương",
   "address": "Thôn 11 xã Hải Đông, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5394395,
   "Latitude": 107.8835642
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Huấn Oanh - Công Ty TNHH Dp Hồng Dương",
   "address": "Thôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.064797,
   "Latitude": 106.5268278
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hải Chi - Công Ty TNHH Dp Hồng Dương",
   "address": "Khu 2, thị trấn Ba Chẽ, huyện Ba Chẽ, tỉnh Quảng Ninh",
   "Longtitude": 21.2718654,
   "Latitude": 107.2836989
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 8 - Công Ty TNHH Dp Hồng Dương",
   "address": "Phố Đông Tiến, thị trấn Tiên Yên, huyện Tiên yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3347973,
   "Latitude": 107.3937536
 },
 {
   "Name": "Quầy thuốc Số 21 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 90 ,  đường Cao Thắng, phường Trần Hưng Đạo, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9570425,
   "Latitude": 107.0856728
 },
 {
   "Name": "Quầy thuốc Số 38 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Tổ 64 khu Bình Minh, phường Cẩm Bình, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0006682,
   "Latitude": 107.2868586
 },
 {
   "Name": "Quầy thuốc Số 6 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 42 ,  đường Trần Phú, phường Trần Phú, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.532385,
   "Latitude": 107.9713391
 },
 {
   "Name": "Quầy thuốc Số 34 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 214 ,  đường Đồng Đăng, phường Việt Hưng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9955245,
   "Latitude": 106.9670788
 },
 // {
 //   "Name": "Quầy thuốc Số 45 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
 //   "address": "Số 251 ,  đường Đồng Đăng, phường Việt Hưng, thành phố Hạ Long, tỉnh Quảng Ninh",
 //   "Longtitude": 21.9614339,
 //   "Latitude": 106.702825
 // },
 {
   "Name": "Quầy thuốc Số 50 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 222, tổ 8A khu 3, phường Hùng Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9576774,
   "Latitude": 107.0007987
 },
 {
   "Name": "Quầy thuốc Số 133 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Thôn 1, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0797764,
   "Latitude": 106.5145667
 },
 {
   "Name": "Quầy thuốc Số 132 - Công Ty Cổ Phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Ki ốt chợ Cột Đông Triều, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0834047,
   "Latitude": 106.5127441
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 133 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 1, xã Tiến Tới, huyện Hải Hà, tỉnh Quảng Ninh",
   "Longtitude": 21.4035334,
   "Latitude": 107.665927
 },
 {
   "Name": "Quầy thuốc Hải Bình- Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Mễ Xá, xã Hưng Đạo, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.064797,
   "Latitude": 106.5268278
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 11 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Xóm Sen, xã Tiền An, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9334638,
   "Latitude": 106.8414374
 },
 {
   "Name": "Quầy thuốc Thu Hoài - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Nhuệ Hổ, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.070062,
   "Latitude": 106.5553799
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Gia Chi - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số nhà 111, tổ 3 khu 10, thị trấnTrới, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0551849,
   "Latitude": 107.0405167
 },
 {
   "Name": "Công ty Cổ phần Dược phẩm Sunli",
   "address": "Km 3 khu 7, phường Hải Yên, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.5244675,
   "Latitude": 107.91574
 },
 {
   "Name": "Nhà thuốc Hà Thơ - Công ty Cổ phần Dược phẩm Sunli",
   "address": "Khu 7, phường Hải Yên, thành Phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.534966,
   "Latitude": 107.948827
 },
 {
   "Name": "Quầy thuốc DN số 54- Cty TNHHDP Bạch Đằng",
   "address": "Thôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0475916,
   "Latitude": 106.6818326
 },
 {
   "Name": "Quầy thuốc DN số 54- Cty TNHHDP Bạch Đằng",
   "address": "Thôn Vĩnh Thái, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0475916,
   "Latitude": 106.6818326
 },
 {
   "Name": "Quầy thuốc DN số 43 - Cty TNHHDP Bạch Đằng",
   "address": "Thôn Xuân Quang, xã Yên Thọ, huyện Đông Triều, Q.Ninh",
   "Longtitude": 21.0567778,
   "Latitude": 106.6202887
 },
 {
   "Name": "Quầy thuốc DN số 4 - Cty Công ty CP Dược VTYT Quảng Ninh",
   "address": "Ki ốt số 121, chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9479461,
   "Latitude": 107.1295067
 },
 {
   "Name": "Quầy thuốc DN số 11 - Cty Công ty CP Dược VTYT Quảng Ninh",
   "address": "Tổ 37 khu 4, phường Cao Thắng, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.966594,
   "Latitude": 107.096763
 },
 {
   "Name": "Cơ Sở Hương Giang",
   "address": "Ki ốt số 7 Chợ Cẩm Đông, phường Cẩm Đông, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Trần Thị Len",
   "address": "Ki ốt số 26 chợ Cẩm Đông, phường Cẩm Đông, thành phố cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Cơ Sở Phúc Trường",
   "address": "Ki ốt số 178 +165 tầng trệt chợ Hồng Hà, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9479461,
   "Latitude": 107.1295067
 },
 {
   "Name": "Nhà Thuốc Tuấn Thủy - Công Ty Cổ phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Số nhà 38, đường Minh Hà, phường Hà Tu, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9565441,
   "Latitude": 107.1608681
 },
 {
   "Name": "Công ty Cổ phần Dược phẩm Thành Đức",
   "address": "Số nhà 384 ,  Đường Trần Phú, phường Cẩm Trung, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0097891,
   "Latitude": 107.270649
 },
 {
   "Name": "Nhà thuốc Thúy Hằng",
   "address": "Số nhà 103, tổ 2 khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Nhà thuốc Hùng Loan",
   "address": "Khu Biểu Nghi, phường Đông Mai, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9980414,
   "Latitude": 106.8440285
 },
 {
   "Name": "Nhà thuốc Minh Trang",
   "address": "Số nhà 438, tổ 2 khu 6B, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9499748,
   "Latitude": 107.1041465
 },
 {
   "Name": "Nhà thuốc Lâm Phương - Công ty CP Dược VTYT Q.Ninh",
   "address": "Số nhà 283 ,  Đường Cao Xanh, Phường Cao Xanh, Thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9760789,
   "Latitude": 107.074614
 },
 {
   "Name": "Nhà thuốc số 1- Công ty Cp thương mại và Dược phẩm Nam Việt",
   "address": "Số nhà 234,  Đường Nguyễn Văn Cừ, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9557688,
   "Latitude": 107.0980769
 },
 {
   "Name": "Nhà thuốc Hùng Mạnh 2 - Công ty CP Dược VTYT Q.Ninh",
   "address": "Số nhà 574, phố Trần Nhân Tông, phường Thanh Sơn, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0344857,
   "Latitude": 106.7522579
 },
 {
   "Name": "Nhà thuốc doanh nghiệp Tân An - Cty TNHH DP Bạch Đằng",
   "address": "Ki ốt 27+28 chợ Trung Tâm Mạo Khê, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Quầy thuốc số 9 - Công Ty CP Dược VTYT Q.Ninh",
   "address": "Số nhà 113, tổ 1 khu 1, Phường Hà Trung, Thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Quầy thuốc doanh nghiệp số 41 - Công ty TNHH DP Hạ Long",
   "address": "Thôn 1 xã Hồng Thái Tây, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0500496,
   "Latitude": 106.6586052
 },
 {
   "Name": "Quầy thuốc doanh nghiệp số 58 - Công ty TNHH DP Hạ Long",
   "address": "Số nhà 168 ,  tổ 2 khu Cầu Trắng, phường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9510629,
   "Latitude": 107.1350169
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Ánh Tuyết- Công ty TNHH DP Hạ Long",
   "address": "Tổ 4 khu Quỳnh Trung, phường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.983019,
   "Latitude": 106.9078245
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Thảo vân- Công ty TNHH DP Hạ Long",
   "address": "Thôn Tràng Bạch, Xã Hoàng Quế, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0549597,
   "Latitude": 106.642214
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Hoa Hồng- Công ty TNHH DP Hạ Long",
   "address": "Số nhà 83 ,  phố 1, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.058394,
   "Latitude": 106.5934706
 },
 {
   "Name": "Quầy thuốc doanh nghiệp số 125- Công ty TNHH DP Hạ Long",
   "address": "Thôn Xuân Bình, xã Bình Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.1054044,
   "Latitude": 106.5854143
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Việt Dung- Công ty TNHH DP Hạ Long",
   "address": "Khu Bình Quyền, thị trấn Bình Liêu, huyện Bình Liêu, tỉnh Quảng Ninh",
   "Longtitude": 21.5248107,
   "Latitude": 107.4033045
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Hùng Hường- Công ty TNHH DP Hạ Long",
   "address": "Số nhà 99, khu Vĩnh Hòa, Thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0598621,
   "Latitude": 106.5869951
 },
 {
   "Name": "Quầy thuốc doanh nghiệp số 118- Công ty TNHH DP Hạ Long",
   "address": "Ki ốt số 189 chợ Đầm Hà, thị trấn Đầm Hà, huyện Đầm Hà, tỉnh Quảng Ninh",
   "Longtitude": 21.3500114,
   "Latitude": 107.5992193
 },
 {
   "Name": "Nhà thuốc Hồng Huy",
   "address": "Tổ 8 khu Lạc Thanh, phường Yên Thanh, Tphường  Uông Bí, Quảng Ninh",
   "Longtitude": 21.0184158,
   "Latitude": 106.7541075
 },
 {
   "Name": "Nhà thuốc Bệnh Viện Đa Khoa Khu Vực Tiên Yên",
   "address": "Bệnh viện ĐKKV Tiên Yên, phố Lý Thường Kiệt, Thị Trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3325573,
   "Latitude": 107.4019703
 },
 {
   "Name": "Công Ty Tnhh Dược Phẩm Thương Mại Đức Dung",
   "address": "Số nhà 32 lô B4 khu đô thị Bãi Muối, phường Cao Thắng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9631743,
   "Latitude": 107.100852
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 45 - Công Ty Tnhh Dược Phẩm Bạch Đằng",
   "address": "Số 169 Lý Thường Kiệt, thị trấn Tiên Yên, huyện Tiên Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3301782,
   "Latitude": 107.398579
 },
 {
   "Name": "Nhà thuốc Tuấn Thủy - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Số 359 Đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9585581,
   "Latitude": 107.1526733
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Tiến Thành - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Tổ 6 khu Vĩnh Tuy 2, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0455727,
   "Latitude": 106.5968968
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Khánh Tâm - Công Ty Tnhh Dược Phẩm Hạ Long",
   "address": "Thôn Bình Lục Thượng, xã Hồng Phong, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0743131,
   "Latitude": 106.5024344
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 12 - Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Xóm 3, xã Hiệp Hòa, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9541241,
   "Latitude": 106.7820375
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Khánh Hà- Công Ty Cổ Phần Thương Mại Và Dược Phẩm Nam Việt",
   "address": "Thôn Tân Lập, xã Tân Yên, huyện Đông Triều , tỉnh Quảng Ninh",
   "Longtitude": 21.0354236,
   "Latitude": 106.6748181
 },
 {
   "Name": "Nhà thuốc Hoài Nam",
   "address": "Số 367 Đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0118793,
   "Latitude": 107.2790011
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Quảng La - Công Ty Tnhh Dược Phẩm Hạ Long",
   "address": "Thôn 5 xã Quảng La, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0866623,
   "Latitude": 106.9027877
 },
 {
   "Name": "Nhà thuốc Khánh Ngọc",
   "address": "Số 192, Tổ 5 Khu 3 Phường Hà Tu, Thành Phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9546746,
   "Latitude": 107.1425442
 },
 {
   "Name": "Nhà thuốc Bảo Yến",
   "address": "Số 141, Tổ 2 Khu Cao Sơn 2, Phường Cao Sơn, Thành phố Cẩm Phả, Tỉnh Quảng Ninh",
   "Longtitude": 21.0037902,
   "Latitude": 107.312133
 },
 {
   "Name": "Nhà thuốc Huy Hà",
   "address": "Số nhà 188, Tổ 2 Khu 5, Phường Yết Kiêu, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9610438,
   "Latitude": 107.0772301
 },
 {
   "Name": "Nhà thuốc Ngọc Diệp",
   "address": "Số 9 Tổ 1 Khu 3, Phường Hà Lầm, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Quầy thuốc số 29 - Công Ty CP Dược VTYT Q.Ninh",
   "address": "Số 10 Tổ 3 khu 6, Phường Bãi Cháy, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9590927,
   "Latitude": 107.0728813
 },
 {
   "Name": "Quầy thuốc số 78 - Công Ty CP Dược VTYT Q.Ninh",
   "address": "Ki ốt số 1 chợ Giếng Đáy, Phường Giếng Đáy, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9775891,
   "Latitude": 107.0084098
 },
 {
   "Name": "Quầy thuốc số 124 - Công Ty CP Dược VTYT Q.Ninh",
   "address": "Ô số 5 ,  ki ốt chợ cột 3, Phường Hồng Hải, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9542838,
   "Latitude": 107.1021962
 },
 {
   "Name": "Nhà thuốc Trường Thủy",
   "address": "Số nhà 184, Tổ 3 Khu 9, Phường Bãi Cháy, Thành phố Hạ Long, Tỉnh Quảng Ninh",
   "Longtitude": 20.9643837,
   "Latitude": 107.0480378
 },
 {
   "Name": "Quầy thuốc số 72 - Công Ty CP Dược VTYT Q.Ninh",
   "address": "Số 34,  Đường Tuệ Tĩnh, Phường Ka Long, Thành phố Móng Cái",
   "Longtitude": 21.5251109,
   "Latitude": 107.9622663
 },
 {
   "Name": "Cơ sở kinh doanh thuốc YHCT Kim Ngân",
   "address": "Thôn Yên Trung, Xã Yên Thọ, Huyện Đông Triều, Tỉnh Quảng Ninh",
   "Longtitude": 21.0526729,
   "Latitude": 106.6220499
 },
 {
   "Name": "Cơ sở kinh doanh thuốc YHCT Minh Đức",
   "address": "Chợ Trung Tâm Mạo Khê, Huyện Đông Triều, Tỉnh Quảng Ninh",
   "Longtitude": 21.0675098,
   "Latitude": 106.5996595
 },
 {
   "Name": "Cơ sở kinh doanh thuốc YHCT",
   "address": "Ki ốt số 35+36,  Tầng 1 Chợ Rừng, Thị xã Quảng Yên, Tỉnh Quảng Ninh",
   "Longtitude": 20.9392012,
   "Latitude": 106.7991686
 },
 {
   "Name": "Cơ Sở thuốc Y học cổ truyền Minh Phắn",
   "address": "Số 31B,  Đường Hùng Vương, Thành Phố Móng Cái, Tỉnh Quảng Ninh",
   "Longtitude": 21.5313213,
   "Latitude": 107.9591522
 },
 {
   "Name": "Cơ sở thuốc Y học cổ truyền Lâm Đông",
   "address": "Số 585 tầng 3 Chợ Trung Tâm, Thành phố Móng Cái, Tỉnh Quảng Ninh",
   "Longtitude": 21.5312507,
   "Latitude": 107.9682761
 },
 {
   "Name": "Cơ sở thuốc Y học cổ truyền An Sinh Đường",
   "address": "Số 63 Đường Hùng Vương, Thành phố Móng Cái, Tỉnh Quảng Ninh",
   "Longtitude": 21.5278509,
   "Latitude": 107.9703632
 },
 {
   "Name": "Nhà Thuốc Minh Nguyệt 2",
   "address": "Số 320, đường Trần Phú, phường Cẩm Thành, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0118793,
   "Latitude": 107.2790011
 },
 {
   "Name": "Nhà Thuốc Tư Nhân Tâm An",
   "address": "Số nhà 755 Trần Phú, tổ 63 khu Tân Lập 1, phường Cẩm Thủy, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0098631,
   "Latitude": 107.2636695
 },
 {
   "Name": "Nhà Thuốc Hằng Ngọc 2",
   "address": "Số 23, tổ 1 khu 1, phường Thanh Sơn, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0934902,
   "Latitude": 106.7935031
 },
 {
   "Name": "Nhà Thuốc Phương Liên",
   "address": "Số 161, tổ 2 khu 4, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9815272,
   "Latitude": 107.0214647
 },
 {
   "Name": "Quầy Thuốc Tiên Lãng - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Thác Bưởi 1, xã Tiên Lãng, huyện Tiên Yên, Quảng Ninh",
   "Longtitude": 21.3045964,
   "Latitude": 107.4332397
 },
 {
   "Name": "Quầy Thuốc Doanh Nghiệp Hiếu Phương- Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 77 thôn Kim Thành, xã Kim Sơn, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0958153,
   "Latitude": 106.6055534
 },
 {
   "Name": "Quầy Thuốc Doanh Nghiệp Lê Thúy - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 46 Phố Trần Bình Trọng, thị trấn Quảng Hà, huyện Hải Hà, Quảng Ninh",
   "Longtitude": 21.452286,
   "Latitude": 107.7521534
 },
 {
   "Name": "Nhà thuốc Kênh Liêm",
   "address": "Số 14, tổ 1 khu 1A, phường Hồng Hải thành phố Hạ Long",
   "Longtitude": 20.9499748,
   "Latitude": 107.1041465
 },
 {
   "Name": "Nhà thuốc Kim Chi",
   "address": "Ki ốt số 17, chợ Hạ Long1, phường  bạch Đằng, TPHL",
   "Longtitude": 20.9506862,
   "Latitude": 107.082389
 },
 {
   "Name": "Quầy thuốc doanh nghiệp số 40- C.ty CP Dược VTYTQN",
   "address": "Số 21, phố Tre Mai, Phường Nam Khê, Uông Bí QN",
   "Longtitude": 21.0251356,
   "Latitude": 106.808365
 },
 {
   "Name": "Quầy thuốc số 16- Cty CP Dược VTYTQN",
   "address": "Khu 7, Phường Hải Yên, TP Móng Cái",
   "Longtitude": 21.5300965,
   "Latitude": 107.927503
 },
 {
   "Name": "Quầy thuốc số 131- C .ty CP Dược VTYT QN",
   "address": "Ki ốt 23,24 chợ Mạo Khê, TTr Mạo Khê, huyện  Đông Triều, QN",
   "Longtitude": 21.0558636,
   "Latitude": 106.603251
 },
 {
   "Name": "Nhà thuốc số 1- Cty CP Dược VTYT QN",
   "address": "12,  Lê Quý Đôn, Hạ Long QN",
   "Longtitude": 20.9505075,
   "Latitude": 107.0808706
 },
 {
   "Name": "Quầy thuốc DN số 2- C.ty CP Dược VTYTQN",
   "address": "Tổ 1 Khu 1, phường  Hà Khánh, Hạ Long, QN",
   "Longtitude": 21.011095,
   "Latitude": 107.129381
 },
 {
   "Name": "Nhà thuốc số 1B- Cty TNHHDP Bạch Đằng",
   "address": "Số 2 Đường Lê Lai, phường  Yết Kieu, Hạ Long QN",
   "Longtitude": 20.9586794,
   "Latitude": 107.0851243
 },
 {
   "Name": "Quầy thuốc doanh nghiêp số 21- Cty TNHHDP Bạch Đằng",
   "address": "Tổ 11K1, phường  Quang Hanh, TP Cẩm Phả QN",
   "Longtitude": 21.0082107,
   "Latitude": 107.2271147
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số1- Cty TNHHDP Bạch Đằng",
   "address": "Số 425, Lê Thánh Tông, phường  Bạch Đằng, Hạ Long QN",
   "Longtitude": 20.9514558,
   "Latitude": 107.0820234
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 1C- Cty TNHHDP Bạch Đằng",
   "address": "Số 80 Đường Tô Hiệu, phường  Cẩm Trung, TP Cẩm Phả QN",
   "Longtitude": 21.0070716,
   "Latitude": 107.2699703
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 5- Cty TNHHDP Bạch Đằng",
   "address": "Tổ 159 Khu 6, phường  Quang Trung, TP Uông Bí QN",
   "Longtitude": 21.1057847,
   "Latitude": 106.8075136
 },
 {
   "Name": "Quầy thuốc số 129- Cty CP Dược VTYTQN",
   "address": "Ki ốt 25,26 chợ Mạo Khê, TTr Mạo Khê, huyện  Đông Triều, QN",
   "Longtitude": 21.0558636,
   "Latitude": 106.603251
 },
 {
   "Name": "Công ty TNHHTM Mỹ Hoa",
   "address": "Số 30,  Tô Hiến Thành, phường  Trần Hưng Đạo. Hạ Long QN",
   "Longtitude": 20.9576657,
   "Latitude": 107.0846437
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Kiên Thuận- Cty TNHHDP Hồng Dương",
   "address": "Số 563,  Tổ 2 khu 4,  TTr Cái Rồng, huyện  Vân Đồn QN",
   "Longtitude": 21.053875,
   "Latitude": 107.4351532
 },
 {
   "Name": "Quầy thuốc số 6- Cty TNHHDP Hồng Dương",
   "address": "Số 32,  Khu Vĩnh Thông,  TTr Mạo Khê,  Đông Triều Quảng Ninh",
   "Longtitude": 21.0568822,
   "Latitude": 106.5982832
 },
 {
   "Name": "Quầy thuốc doanh nghiệp Khiêm Thủy- Cty TNHHDP Hồng Dương",
   "address": "Thôn Bằng Xăm, Xã Lê Lợi, huyện  Hoành Bồ, QN",
   "Longtitude": 21.0169087,
   "Latitude": 107.0421324
 },
 {
   "Name": "Nhà thuốc Hưng Hà- Công ty TNHHDP Hồng Dương",
   "address": "Số 544,  Tuệ Tĩnh, phường  Thanh Sơn Uông Bí QN",
   "Longtitude": 21.0374998,
   "Latitude": 106.7516414
 },
 {
   "Name": "Quầy thuốc Hải Tiến-Công ty TNHHDP Hồng Dương",
   "address": "Thôn 8 Km 13 xã Hải Tiến, TP Móng Cái, QN",
   "Longtitude": 21.5394988,
   "Latitude": 107.8569058
 },
 {
   "Name": "Quầy thuốc Ngân Thương- Công ty TNHHDP Hồng Dương",
   "address": "Số nhà 48, Khu 5, Phố Đầm Buôn, TTr Ba Chẽ, QN",
   "Longtitude": 21.294325,
   "Latitude": 107.1484521
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Linh Loan - Công TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Thọ Sơn, xã Yên Thọ, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.0608783,
   "Latitude": 106.6169661
 },
 {
   "Name": "Quầy thuốcDN Hồng Nhung- C.ty CPTM và DP Nam Việt",
   "address": "SN 144, Phố Nguyễn Bình, TT Đông Triều,Quảng Ninh",
   "Longtitude": 21.0832498,
   "Latitude": 106.511486
 },
 {
   "Name": "Quầy thuốc số 1- Công ty CP Dược VTYT Quảng Ninh",
   "address": "Số 112, Đường Bãi Muối, phường Cao Thắng, TP Hạ Long",
   "Longtitude": 20.9687487,
   "Latitude": 107.0970687
 },
 {
   "Name": "Công ty CP thương mại và Dược phẩm Nam Việt",
   "address": "Số 67,  Phố Hải Lộc, phường  Hồng Hải, TPHL,QN",
   "Longtitude": 20.9469525,
   "Latitude": 107.105489
 },
 {
   "Name": "Nhà Thuốc Hồng Dương 1- C.ty TNHHDP Hồng Dương",
   "address": "Số 83 tổ 4 khu 1A,phường Cao Thắng TPHL,QN",
   "Longtitude": 20.966594,
   "Latitude": 107.096763
 },
 {
   "Name": "Nhà Thuốc Xuân Thu- Công ty TNHHDP Hạ Long",
   "address": "SN 41,  Phố Nguyễn Du, phường Quang Trung, Uông Bí, Quảng Ninh",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "Name": "Nhà thuốc Y cao Hà Nội",
   "address": "Số 907,  Trần Phú phường Cẩm Thạch,TP cẩm Phả Quảng Ninh",
   "Longtitude": 21.0099567,
   "Latitude": 107.2570463
 },
 {
   "Name": "Nhà Thuốc Vân Hương",
   "address": "SN 48, ĐườngTrần Nhân Tông, Khu1,phường Thanh Sơn, Uông Bí, Quảng Ninh",
   "Longtitude": 21.0934902,
   "Latitude": 106.7935031
 },
 {
   "Name": "Quầy Thuốc Doanh Nghiệp Số 37 - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Tân Yên, xã Hồng Thái Đông, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0391836,
   "Latitude": 106.7020879
 },
 {
   "Name": "Quầy Thuốc Bích Ngọc - Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Xóm Chợ, xã Tiền An, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.9386407,
   "Latitude": 106.8473377
 },
 {
   "Name": "Quầy Thuốc Doanh Nghiệp Công Ngọc - Công Ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Xuân Quang, xã Yên Thọ, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0567778,
   "Latitude": 106.6202887
 },
 {
   "Name": "Quầy Thuốc Chương Xuyến Số 5 - Công Ty TNHH Dược Phẩm Thương Mại Thái Ngọc",
   "address": "Thôn Đồng Cao, xã Thống Nhất, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0351254,
   "Latitude": 107.0893798
 },
 {
   "Name": "Nhà thuốc Hương Giang",
   "address": "Ki ốt 13 chợ Hạ Long 1, phường Bạch Đằng, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9506862,
   "Latitude": 107.082389
 },
 {
   "Name": "Nhà thuốc Ngân Sơn 2",
   "address": "SN 46 tổ 77 khu 7, phường  Hà Khẩu, thành phố Hạ Long",
   "Longtitude": 20.9736092,
   "Latitude": 106.9860384
 },
 {
   "Name": "Công ty TNHHDP Hạ Long",
   "address": "Số 08 Phố Hoàng Long, phường Bạch Đằng, thành phố Hạ Long",
   "Longtitude": 20.9508443,
   "Latitude": 107.0834804
 },
 {
   "Name": "Nhà thuốc Trung Tâm- Công ty CP Dược VTYT Q.Ninh",
   "address": "Số 703,  Lê Thánh Tông ,  TP Hạ Long",
   "Longtitude": 20.9536147,
   "Latitude": 107.090891
 },
 {
   "Name": "NHÀ THUỐC LAN ANH",
   "address": "Số nhà 82 Đường Nguyễn Văn Cừ, phường Hồng Hà, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9557688,
   "Latitude": 107.0980769
 },
 {
   "Name": "Quầy thuốc doanh nghiệp",
   "address": "Thôn Xuân Bình, xã Bình Khê ,  huyện Đông Triều ,  Quảng Ninh",
   "Longtitude": 21.1054044,
   "Latitude": 106.5854143
 },
 {
   "Name": "Quầy thuốc doanh nghiệp",
   "address": "Thôn 8, xã Hải Đông,  TP Móng Cái ,  Quảng Ninh",
   "Longtitude": 21.5394395,
   "Latitude": 107.8835642
 },
 {
   "Name": "Quầy thuốc doanh nghiệp",
   "address": "Thôn Thái Lập, xã Tân Lập,  Huyện Đầm Hà ,  Quảng Ninh",
   "Longtitude": 21.2762139,
   "Latitude": 107.5919395
 },
 {
   "Name": "Nhà thuốc doanh nghiệp số 01- C.ty TNHHDP Hạ Long",
   "address": "Số 8 phố Hoàng Long phường  Bạch Đằng TP Hạ Long",
   "Longtitude": 20.9508443,
   "Latitude": 107.0834804
 },
 // {
 //   "Name": "Quầy thuốc số 28- C.ty TNHHDP Hải Bình",
 //   "address": "SN 260 tổ 4 khu Bắc Sơn, phường  Cẩm Sơn, TP Cẩm Phả",
 //   "Longtitude": 21.8437729,
 //   "Latitude": 106.2992912
 // },
 {
   "Name": "Hộ kinh doanh thuốc đông y Đàm Oanh",
   "address": "Số nhà 24,  Phố Lê Hoàn, phường  Bạch Đằng, TP Hạ Long",
   "Longtitude": 20.9509759,
   "Latitude": 107.0819854
 },
 {
   "Name": "Nhà thuốc Minh Hương- C.ty TNHHDP huyện Long",
   "address": "SN 400 tổ 64 khu 6 phường Cửa Ông, TP Cẩm Phả",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Công ty TNHHDP Bạch Đằng",
   "address": "Tổ 36 khu 2 P Bạch Đằng, TP Hạ Long",
   "Longtitude": 20.9513835,
   "Latitude": 107.0871951
 },
 {
   "Name": "Quầy thuốc số 56- C.ty CP Dược VTYTQN",
   "address": "Ô B21 Chợ Trung Tâm Uông Bí,  TP Uông Bí",
   "Longtitude": 21.0347844,
   "Latitude": 106.769557
 },
 {
   "Name": "Quầy thuốc số 130- C.ty CP Dược VTYTQN",
   "address": "Khu Bình An, TT Bình Liêu, H Bình Liêu",
   "Longtitude": 21.5258306,
   "Latitude": 107.3957643
 },
 {
   "Name": "Quầy thuốc số 123- C.ty CP Dược VTYTQN",
   "address": "Tổ 6 khu 2, phường  Hồng Hà, Tp Hạ Long",
   "Longtitude": 20.9443113,
   "Latitude": 107.1225746
 },
 {
   "Name": "Quầy thuốcDN Hương Giang - C. ty TNHHDP huyện Long",
   "address": "Tổ 1 khu 4 Phường Đại Yên thành phố Hạ Long",
   "Longtitude": 20.9731011,
   "Latitude": 106.9322437
 },
 {
   "Name": "Nhà thuốc Minh Huyền",
   "address": "SN 68B Tổ 6 khu 4, Đường Bái Tử Long, phường Cẩm Trung, thành phố Cẩm Phả.",
   "Longtitude": 21.0059966,
   "Latitude": 107.2724323
 },
 {
   "Name": "Quầy thuốc số 123- C.ty CP Dược VTYTQN",
   "address": "Tổ 6 khu 2, phường  Hồng Hà, Tp Hạ Long",
   "Longtitude": 20.9443113,
   "Latitude": 107.1225746
 },
 {
   "Name": "Công Ty Cổ Phần Công Nghệ Xanh Đông Sơn",
   "address": "Số 46 phố Anh Đào, Tổ 8 khu 2, phường Bãi Cháy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9579074,
   "Latitude": 107.0226806
 },
 {
   "Name": "Nhà Thuốc Bình Huê",
   "address": "Số nhà 579, tổ 1 khu 4B, phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0082107,
   "Latitude": 107.2271147
 },
 {
   "Name": "Nhà Thuốc Tuấn Hoài",
   "address": "Ki ốt B27, chợ Trung tâm Hải Hà, huyện Hải Hà, tỉnh Quảng Ninh",
   "Longtitude": 21.4498682,
   "Latitude": 107.7493787
 },
 {
   "Name": "Nhà Thuốc Vân Trang",
   "address": "Số nhà 09 tổ 2 khu 3, phường Hồng Hà, Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9417785,
   "Latitude": 107.1277751
 },
 {
   "Name": "Quầy Thuốc Số 135- Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Xóm Bấc, xã Liên Vị, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.9334638,
   "Latitude": 106.8414374
 },
 {
   "Name": "Công Ty TNHH Một Thành Viên Dược Phẩm Trung Ương I- Chi Nhánh Quảng Ninh",
   "address": "Số 146, ngõ 3,  Đường Cao Thắng, phường Cao Thắng Tphường Hạ Long, Quảng Ninh",
   "Longtitude": 20.9628713,
   "Latitude": 107.0962345
 },
 {
   "Name": "Quầy Thuốc Phương Thúy- Công Ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn Bình Lục Thượng, xã Hồng Phong, huyện Đông Triều,Quảng Ninh",
   "Longtitude": 21.0743131,
   "Latitude": 106.5024344
 },
 {
   "Name": "Nhà Thuốc Thảo Hiền",
   "address": "Tổ 69B khu 6, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9793434,
   "Latitude": 107.0849499
 },
 {
   "Name": "Nhà Thuốc Hồng Hà - Công Ty Cổ Phần Dược Vtyt Quảng Ninh",
   "address": "Số 415A Đường Nguyễn văn Cừ, phường Hồng Hà, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9443086,
   "Latitude": 107.1225384
 },
 {
   "Name": "Nhà Thuốc Số 1 - Công Ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 71 đường 25/4, tổ 36 khu 2, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9502012,
   "Latitude": 107.0879032
 },
 {
   "Name": "Chi nhánh công ty cổ phần Traphaco",
   "address": "Ô số 1 Lô A6 Khu Đô thị mới Cao Xanh, P Hà Khánh, thành phố Hạ Long",
   "Longtitude": 20.9643795,
   "Latitude": 107.0846676
 },
 {
   "Name": "Nhà thuốc Quý Kiên",
   "address": "Số 330 Đường Nguyễn Văn Cừ, P Hồng Hải,thành phố Hạ Long",
   "Longtitude": 20.9495017,
   "Latitude": 107.1068609
 },
 {
   "Name": "Quầy thuốc doanh nghiêp Hồng Phong",
   "address": "Tổ 49 C khu 6, phường Cẩm Thành, TP Cẩm Phả",
   "Longtitude": 21.0113744,
   "Latitude": 107.2769837
 },
 {
   "Name": "Đại lý thuốc số 23",
   "address": "Thôn An Biên 2, xã Lê Lợi, huyện Hoành Bồ",
   "Longtitude": 21.0207142,
   "Latitude": 107.014153
 },
 {
   "Name": "Quầy thuốc DN số 66- C.Ty TNHHDPHL",
   "address": "Khu Đường Ngang, phường  Minh Thành, Tx Quảng Yên",
   "Longtitude": 21.00493,
   "Latitude": 106.8552697
 },
 {
   "Name": "Quầy Thuốc Số 43 - Công Ty Cổ phần Dược Vật Tư Y Tế Quảng Ninh",
   "address": "Số 144B ,  Hùng Vương, phường Ka Long, thành phố Móng Cái, tỉnh Quảng Ninh",
   "Longtitude": 21.531239,
   "Latitude": 107.9596089
 },
 {
   "Name": "Nhà Thuốc Minh Tâm 1",
   "address": "Số nhà 1139 Đường Trần Phú, Phường Cẩm Thạch, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0101542,
   "Latitude": 107.2539561
 },
 {
   "Name": "Quầy Thuốc Số 95 - Công Ty Cổ phần Dược Vtyt Quảng Ninh",
   "address": "Số 44, đường Quang Trung, thị trấn Tiên Yên, huyện Tiên Yên, Quảng Ninh",
   "Longtitude": 21.3347973,
   "Latitude": 107.3937536
 },
 {
   "Name": "Nhà Thuốc Hoàng Sinh",
   "address": "Số 275 Đường Trần Quốc Tảng,phường cẩm Thịnh, TP Cẩm Phả",
   "Longtitude": 21.0080058,
   "Latitude": 107.3445192
 },
 {
   "Name": "Nhà Thuốc Thắng Hạnh",
   "address": "Số 104 Nguyễn Bình, phường Quảng Yên, thị xã Quảng Yên, Quảng Ninh",
   "Longtitude": 20.9392509,
   "Latitude": 106.8137728
 },
 {
   "Name": "Quầy Thuốc Số 68 - Công Ty Cp Dược Vtyt Quảng Ninh",
   "address": "Số 494 Đường Nguyễn Văn Cừ, phường Hồng Hà, Tphường  Hạ Long, Quảng Ninh",
   "Longtitude": 20.9482282,
   "Latitude": 107.1293532
 },
 {
   "Name": "Công ty TNHHMTV Thảo Châm",
   "address": "SN 789 Đường Nguyễn Văn Cừ,  phường Hồng Hải Tphường  HL",
   "Longtitude": 20.9481232,
   "Latitude": 107.1074036
 },
 {
   "Name": "Quầy thuốc Thảo Châm",
   "address": "SN 789 Đường Nguyễn Văn Cừ,  phường  Hồng Hải thành phố Hạ Long",
   "Longtitude": 20.9481232,
   "Latitude": 107.1074036
 },
 {
   "Name": "Nhà thuốc Hương Xuyên",
   "address": "Tổ 108 K6,  phường  B Đằng , thành phố Hạ Long",
   "Longtitude": 20.9711977,
   "Latitude": 107.0448069
 },
 {
   "Name": "Nhà thuốc Hồng Dương - C.ty TNHHDP Hồng Dương",
   "address": "Số 1, Tổ 1 khu 1,phường Trần Hưng Đạo,  thành phố Hạ Long,Quảng Ninh",
   "Longtitude": 20.9533239,
   "Latitude": 107.0871119
 },
 {
   "Name": "Nhà thuốc Đông Đô - Công ty Cổ phần Y Tế Đông Đô",
   "address": "Số 315 Cầu sến, phường Yên Thanh, Tphường  Uông Bí, Quảng Ninh",
   "Longtitude": 21.0352525,
   "Latitude": 106.7409083
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Quang Minh- Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Khu Yên Hợp, xã Yên Thọ, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0585004,
   "Latitude": 106.6122025
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 18 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Đồng Tâm, xã Lê Lợi, huyện Hoành Bồ, Quảng Ninh",
   "Longtitude": 21.0195827,
   "Latitude": 107.0358077
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 4 Linh Chi - Công ty TNHH Dược Phẩm Thương Mại Thái Ngọc",
   "address": "Thôn 4, xã Sông Khoai, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9768581,
   "Latitude": 106.8119382
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thảo Hiền - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 3, xã Đức Chính, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.08051,
   "Latitude": 106.5256786
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 112 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Đông Hải, xã Đông Xá, huyện Vân Đồn, tỉnh Quảng Ninh",
   "Longtitude": 21.0600411,
   "Latitude": 107.4170053
 },
 {
   "Name": "Nhà thuốc Duyên Thùy",
   "address": "Số nhà 384, Đường Trần Phú, phường Cẩm Trung, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0097891,
   "Latitude": 107.270649
 },
 {
   "Name": "Nhà thuốc Số 1A - Công ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Số 12 Lê Quý Đôn, phường Bạch Đằng, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9505075,
   "Latitude": 107.0808706
 },
 {
   "Name": "Nhà thuốc Trung Thành -",
   "address": "Ki ốt số 6 vành đai nhà tôn chợ Giếng Đáy, phường Giếng Đáy, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 24.2225023,
   "Latitude": 120.6545022
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thanh Bình - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Số nhà 265, khu 3, thị trấn Cái Rồng, huyện Vân Đồn, tỉnh Quảng Ninh",
   "Longtitude": 21.053875,
   "Latitude": 107.4351532
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Thanh Huyên 2 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn Cửa Tràng, xã Tiền An, thị xã Quảng Yên, tỉnh Quảng Ninh",
   "Longtitude": 20.9385813,
   "Latitude": 106.8316791
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 75 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Thôn 8, xã Hải Đông, Tphường Móng Cái, Quảng Ninh",
   "Longtitude": 21.5394395,
   "Latitude": 107.8835642
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Ngọc Thủy - Công ty TNHH Dược Phẩm Hồng Dương",
   "address": "Đoàn xã 1, xã Hồng Phong, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0743131,
   "Latitude": 106.5024344
 },
 {
   "Name": "Nhà thuốc Thanh Nhàn 2",
   "address": "Ki ốt 188A Chợ Cẩm Đông, phường Cẩm Đông, thành phố Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.0081551,
   "Latitude": 107.2919065
 },
 {
   "Name": "Nhà thuốc Bệnh Viện Lao Và Phổi",
   "address": "Bệnh viện Lao và Phổi, phường Cao Xanh, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.979084,
   "Latitude": 107.0900325
 },
 {
   "Name": "Nhà thuốc Hương Trang",
   "address": "Tổ 11 khu 4, phường Hà Lầm, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.970539,
   "Latitude": 107.1144837
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 12 - Công ty TNHH Dược Phẩm Bạch Đằng",
   "address": "Địa chỉ Tổ 4 khu 7A, Phường Quang Hanh, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 20.9955298,
   "Latitude": 107.2084093
 },
 {
   "Name": "Nhà thuốc Minh Tuyến",
   "address": "Số nhà 526 ,  Đường Nguyễn Văn Cừ, phường Hồng Hải, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9557688,
   "Latitude": 107.0980769
 },
 {
   "Name": "Nhà thuốc Thanh Thúy",
   "address": "Khu 7, phường Hải Yên, thành phố Móng Cái, quảng Ninh",
   "Longtitude": 21.5259366,
   "Latitude": 107.9279309
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 44 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 25,  Lý Thường Kiệt, thị trấn Tiên Yên, tỉnh Quảng Ninh",
   "Longtitude": 21.3315865,
   "Latitude": 107.4025342
 },
 {
   "Name": "Nhà thuốc Long Hiền",
   "address": "Số 270 Đường Vũ Văn Hiếu, phường Hà Tu, thành phố Hạ Long, Quảng Ninh",
   "Longtitude": 20.9585581,
   "Latitude": 107.1526733
 },
 {
   "Name": "Nhàthuốc Doanh Nghiệp Minh Phương - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 102, tổ 15 khu 3, phường Cửa Ông, Tphường Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Cơ Sở thuốc Y Học Cổ Truyền",
   "address": "Ki ốt số 1 chợ Ba Lan, phường Giếng Đáy, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9814933,
   "Latitude": 107.0232489
 },
 {
   "Name": "Nhà thuốc Số 1 Hà Nội",
   "address": "Số 140 phố 1, phường Mạo Khê, thị xã Đông Triều, Quảng Ninh",
   "Longtitude": 21.058394,
   "Latitude": 106.5934706
 },
 {
   "Name": "Nhà thuốc Hùng Mạnh",
   "address": "Số 238, tổ 12A, khu 4, phường Vàng Danh, thành phố Uông Bí, Quảng Ninh",
   "Longtitude": 21.097721,
   "Latitude": 106.8016144
 },
 {
   "Name": "Nhà thuốc Thanh Nhàn",
   "address": "Số 351, phố Mới, phường  Cửa Ông, Tphường  Cẩm Phả, Quảng Ninh",
   "Longtitude": 21.032741,
   "Latitude": 107.3598781
 },
 {
   "Name": "Nhà thuốc Tâm Đức",
   "address": "Số nhà 63, tổ 1 khu Minh Khai, phường Cẩm Tây, thành phố Cẩm Phả, tỉnh Quảng Ninh",
   "Longtitude": 21.0120178,
   "Latitude": 107.2872176
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Anh Đào - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Khu Dốc Thụ, xã Yên Thọ, huyện Đông Triều, Quảng Ninh",
   "Longtitude": 21.0567778,
   "Latitude": 106.6202887
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Hồng Vân - Công ty TNHH Dược Phẩm Hồng Dương",
   "address": "Thôn 13, xã Hạ Long, huyện Vân Đồn, tỉnh Quảng Ninh",
   "Longtitude": 21.0623461,
   "Latitude": 107.4913489
 },
 {
   "Name": "Quầy thuốc Doanh Nghiệp Số 135 - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Số 183, tổ 1 khu Quang Trung, thị trấn Mạo Khê, huyện Đông Triều, tỉnh Quảng Ninh",
   "Longtitude": 21.0584203,
   "Latitude": 106.5985501
 },
 {
   "Name": "Nhà thuốc Bệnh Viện Sản Nhi",
   "address": "Phường Đại Yên, thành phố Hạ Long, tỉnh Quảng Ninh",
   "Longtitude": 20.9828115,
   "Latitude": 106.9240554
 },
 {
   "Name": "Nhà thuốc Xuân Thu - Công ty TNHH Dược Phẩm Hạ Long",
   "address": "Số nhà 41 phố Nguyễn Du, phường Quang Trung, thành phố Uông Bí, tỉnh Quảng Ninh",
   "Longtitude": 21.0378074,
   "Latitude": 106.7701049
 }
];