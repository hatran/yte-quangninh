var dataptramyte = [
 {
   "STT": 1,
   "Name": "Trạm Y tế Phường Long Thạnh",
   "address": "Phường Long Thạnh, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7708956,
   "Latitude": 105.2838511
 },
 {
   "STT": 2,
   "Name": "Trạm Y tế Phường Long Hưng",
   "address": "Phường Long Hưng, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7958634,
   "Latitude": 105.2308219
 },
 {
   "STT": 3,
   "Name": "Trạm Y tế Phường Long Châu",
   "address": "Phường Long Châu, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7951531,
   "Latitude": 105.2370339
 },
 {
   "STT": 4,
   "Name": "Trạm Y tế Xã Phú Lộc",
   "address": "Xã Phú Lộc, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.9095149,
   "Latitude": 105.1434387
 },
 {
   "STT": 5,
   "Name": "Trạm Y tế Xã Vĩnh Xương",
   "address": "Xã Vĩnh Xương, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8973641,
   "Latitude": 105.1668326
 },
 {
   "STT": 6,
   "Name": "Trạm Y tế Xã Vĩnh Hòa",
   "address": "Xã Vĩnh Hòa, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8528973,
   "Latitude": 105.1785307
 },
 {
   "STT": 7,
   "Name": "Trạm Y tế Xã Tân Thạnh",
   "address": "Xã Tân Thạnh, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8129361,
   "Latitude": 105.191593
 },
 {
   "STT": 8,
   "Name": "Trạm Y tế Xã Tân An",
   "address": "Xã Tân An, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8129361,
   "Latitude": 105.191593
 },
 {
   "STT": 9,
   "Name": "Trạm Y tế Xã Long An",
   "address": "Xã Long An, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7865361,
   "Latitude": 105.1902297
 },
 {
   "STT": 10,
   "Name": "Trạm Y tế Phường Long Phú",
   "address": "Phường Long Phú, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7842885,
   "Latitude": 105.2241812
 },
 {
   "STT": 11,
   "Name": "Trạm Y tế Xã Châu Phong",
   "address": "Xã Châu Phong, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7340814,
   "Latitude": 105.1434387
 },
 {
   "STT": 12,
   "Name": "Trạm Y tế Xã Phú Vĩnh",
   "address": "Xã Phú Vĩnh, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7536668,
   "Latitude": 105.1902297
 },
 {
   "STT": 13,
   "Name": "Trạm Y tế Xã Lê Chánh",
   "address": "Xã Lê Chánh, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.732922,
   "Latitude": 105.1668326
 },
 {
   "STT": 14,
   "Name": "Trạm Y tế Phường Long Sơn",
   "address": "Phường Long Sơn, Thị xã Tân Châu, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7726552,
   "Latitude": 105.248737
 },
 {
   "STT": 15,
   "Name": "Trạm Y tế Phường Mỹ Bình",
   "address": "Phường Mỹ Bình, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3871655,
   "Latitude": 105.4355647
 },
 {
   "STT": 16,
   "Name": "Trạm Y tế Phường Mỹ Long",
   "address": "Phường Mỹ Long, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3792399,
   "Latitude": 105.4434192
 },
 {
   "STT": 17,
   "Name": "Trạm Y tế Phường Đông Xuyên",
   "address": "Phường Đông Xuyên, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3745076,
   "Latitude": 105.4287738
 },
 {
   "STT": 18,
   "Name": "Trạm Y tế Phường Mỹ Xuyên",
   "address": "Phường Mỹ Xuyên, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3775894,
   "Latitude": 105.4347966
 },
 {
   "STT": 19,
   "Name": "Trạm Y tế Phường Bình Đức",
   "address": "Phường Bình Đức, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4174582,
   "Latitude": 105.4115314
 },
 {
   "STT": 20,
   "Name": "Trạm Y tế Phường Bình Khánh",
   "address": "Phường Bình Khánh, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3953269,
   "Latitude": 105.4208821
 },
 {
   "STT": 21,
   "Name": "Trạm Y tế Phường Mỹ Phước",
   "address": "Phường Mỹ Phước, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3696148,
   "Latitude": 105.4464824
 },
 {
   "STT": 22,
   "Name": "Trạm Y tế Phường Mỹ Quý",
   "address": "Phường Mỹ Quý, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.36208,
   "Latitude": 105.4524494
 },
 {
   "STT": 23,
   "Name": "Trạm Y tế Phường Mỹ Thới",
   "address": "Phường Mỹ Thới, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3562421,
   "Latitude": 105.4582046
 },
 {
   "STT": 24,
   "Name": "Trạm Y tế Phường Mỹ Thạnh",
   "address": "Phường Mỹ Thạnh, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3334683,
   "Latitude": 105.4800424
 },
 {
   "STT": 25,
   "Name": "Trạm Y tế Phường Mỹ Hòa",
   "address": "Phường Mỹ Hòa, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3772782,
   "Latitude": 105.4221943
 },
 {
   "STT": 26,
   "Name": "Trạm Y tế Xã Mỹ Khánh",
   "address": "Xã Mỹ Khánh, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3792302,
   "Latitude": 105.3872573
 },
 {
   "STT": 27,
   "Name": "Trạm Y tế Xã Mỹ Hoà Hưng",
   "address": "Xã Mỹ Hoà Hưng, Thành phố Long Xuyên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3990635,
   "Latitude": 105.4426552
 },
 {
   "STT": 28,
   "Name": "Trạm Y tế Phường Châu Phú B",
   "address": "Phường Châu Phú BThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6810551,
   "Latitude": 105.1083542
 },
 {
   "STT": 29,
   "Name": "Trạm Y tế Phường Châu Phú A",
   "address": "Phường Châu Phú AThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7058379,
   "Latitude": 105.1054308
 },
 {
   "STT": 30,
   "Name": "Trạm Y tế Phường Vĩnh Mỹ",
   "address": "Phường Vĩnh MỹThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6869563,
   "Latitude": 105.1398205
 },
 {
   "STT": 31,
   "Name": "Trạm Y tế Phường Núi Sam",
   "address": "Phường Núi SamThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6712546,
   "Latitude": 105.0849687
 },
 {
   "STT": 32,
   "Name": "Trạm Y tế Phường Vĩnh Ngươn",
   "address": "Phường Vĩnh NgươnThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7222677,
   "Latitude": 105.1054308
 },
 {
   "STT": 33,
   "Name": "Trạm Y tế Xã Vĩnh Tế",
   "address": "Xã Vĩnh TếThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6598326,
   "Latitude": 105.0512327
 },
 {
   "STT": 34,
   "Name": "Trạm Y tế Xã Vĩnh Châu",
   "address": "Xã Vĩnh ChâuThành phố Châu Đốc, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6575692,
   "Latitude": 105.1109531
 },
 {
   "STT": 35,
   "Name": "Trạm Y tế Thị trấn Tri Tôn",
   "address": "Thị trấn Tri Tôn, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4236989,
   "Latitude": 105.0031457
 },
 {
   "STT": 36,
   "Name": "Trạm Y tế Thị trấn Ba Chúc",
   "address": "Thị trấn Ba Chúc, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4833465,
   "Latitude": 104.8980058
 },
 {
   "STT": 37,
   "Name": "Trạm Y tế Xã Lạc Quới",
   "address": "Xã Lạc Quới, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5324229,
   "Latitude": 104.8992634
 },
 {
   "STT": 38,
   "Name": "Trạm Y tế Xã Lê Trì",
   "address": "Xã Lê Trì, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.499878,
   "Latitude": 104.9293033
 },
 {
   "STT": 39,
   "Name": "Trạm Y tế Xã Vĩnh Gia",
   "address": "Xã Vĩnh Gia, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5039104,
   "Latitude": 104.8104424
 },
 {
   "STT": 40,
   "Name": "Trạm Y tế Xã Vĩnh Phước",
   "address": "Xã Vĩnh Phước, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4590595,
   "Latitude": 104.8337879
 },
 {
   "STT": 41,
   "Name": "Trạm Y tế Xã Châu Lăng",
   "address": "Xã Châu Lăng, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4461033,
   "Latitude": 104.9914601
 },
 {
   "STT": 42,
   "Name": "Trạm Y tế Xã Lương Phi",
   "address": "Xã Lương Phi, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4474043,
   "Latitude": 104.9248194
 },
 {
   "STT": 43,
   "Name": "Trạm Y tế Xã Lương An Trà",
   "address": "Xã Lương An Trà, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3912866,
   "Latitude": 104.8804893
 },
 {
   "STT": 44,
   "Name": "Trạm Y tế Xã Tà Đảnh",
   "address": "Xã Tà Đảnh, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4031312,
   "Latitude": 105.0908147
 },
 {
   "STT": 45,
   "Name": "Trạm Y tế Xã Núi Tô",
   "address": "Xã Núi Tô, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4013017,
   "Latitude": 105.0148322
 },
 {
   "STT": 46,
   "Name": "Trạm Y tế Xã An Tức",
   "address": "Xã An Tức, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3879754,
   "Latitude": 104.9505671
 },
 {
   "STT": 47,
   "Name": "Trạm Y tế Xã Cô Tô",
   "address": "Xã Cô Tô, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3628179,
   "Latitude": 105.0206757
 },
 {
   "STT": 48,
   "Name": "Trạm Y tế Xã Tân Tuyến",
   "address": "Xã Tân Tuyến, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3376626,
   "Latitude": 105.0908147
 },
 {
   "STT": 49,
   "Name": "Trạm Y tế Xã Ô Lâm",
   "address": "Xã Ô Lâm, Huyện Tri Tôn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3432053,
   "Latitude": 104.9739333
 },
 {
   "STT": 50,
   "Name": "Trạm Y tế Thị trấn Nhà Bàng",
   "address": "Thị trấn Nhà Bàng, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6231047,
   "Latitude": 105.0060673
 },
 {
   "STT": 51,
   "Name": "Trạm Y tế Thị trấn Chi Lăng",
   "address": "Thị trấn Chi Lăng, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5318577,
   "Latitude": 105.0265195
 },
 {
   "STT": 52,
   "Name": "Trạm Y tế Xã Núi Voi",
   "address": "Xã Núi Voi, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5360404,
   "Latitude": 105.0460902
 },
 {
   "STT": 53,
   "Name": "Trạm Y tế Xã Nhơn Hưng",
   "address": "Xã Nhơn Hưng, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6533543,
   "Latitude": 105.0031457
 },
 {
   "STT": 54,
   "Name": "Trạm Y tế Xã An Phú",
   "address": "Xã An Phú, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6216438,
   "Latitude": 104.9797753
 },
 {
   "STT": 55,
   "Name": "Trạm Y tế Xã Thới Sơn",
   "address": "Xã Thới Sơn, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6084301,
   "Latitude": 105.0265195
 },
 {
   "STT": 56,
   "Name": "Trạm Y tế Thị trấn Tịnh Biên",
   "address": "Thị trấn Tịnh Biên, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5905003,
   "Latitude": 104.9447261
 },
 {
   "STT": 57,
   "Name": "Trạm Y tế Xã Văn Giáo",
   "address": "Xã Văn Giáo, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5859787,
   "Latitude": 105.0382076
 },
 {
   "STT": 58,
   "Name": "Trạm Y tế Xã An Cư",
   "address": "Xã An Cư, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5450463,
   "Latitude": 104.9797753
 },
 {
   "STT": 59,
   "Name": "Trạm Y tế Xã An Nông",
   "address": "Xã An Nông, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5639847,
   "Latitude": 104.9272044
 },
 {
   "STT": 60,
   "Name": "Trạm Y tế Xã Vĩnh Trung",
   "address": "Xã Vĩnh Trung, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.549107,
   "Latitude": 105.0089888
 },
 {
   "STT": 61,
   "Name": "Trạm Y tế Xã Tân Lợi",
   "address": "Xã Tân Lợi, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5088656,
   "Latitude": 105.0498966
 },
 {
   "STT": 62,
   "Name": "Trạm Y tế Xã An Hảo",
   "address": "Xã An Hảo, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4708984,
   "Latitude": 105.044052
 },
 {
   "STT": 63,
   "Name": "Trạm Y tế Xã Tân Lập",
   "address": "Xã Tân Lập, Huyện Tịnh Biên, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4634686,
   "Latitude": 105.0849687
 },
 {
   "STT": 64,
   "Name": "Trạm Y tế Thị trấn Núi Sập",
   "address": "Thị trấn Núi Sập, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.2581682,
   "Latitude": 105.2721456
 },
 {
   "STT": 65,
   "Name": "Trạm Y tế Thị trấn Phú Hoà",
   "address": "Thị trấn Phú Hoà, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3534221,
   "Latitude": 105.386309
 },
 {
   "STT": 66,
   "Name": "Trạm Y tế Thị Trấn Óc Eo",
   "address": "Thị Trấn Óc Eo, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.241958,
   "Latitude": 105.1551352
 },
 {
   "STT": 67,
   "Name": "Trạm Y tế Xã Tây Phú",
   "address": "Xã Tây Phú, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3561254,
   "Latitude": 105.1609838
 },
 {
   "STT": 68,
   "Name": "Trạm Y tế Xã An Bình",
   "address": "Xã An Bình, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3125096,
   "Latitude": 105.1609838
 },
 {
   "STT": 69,
   "Name": "Trạm Y tế Xã Vĩnh Phú",
   "address": "Xã Vĩnh Phú, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3527541,
   "Latitude": 105.2311827
 },
 {
   "STT": 70,
   "Name": "Trạm Y tế Xã Vĩnh Trạch",
   "address": "Xã Vĩnh Trạch, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3310367,
   "Latitude": 105.3423909
 },
 {
   "STT": 71,
   "Name": "Trạm Y tế Xã Phú Thuận",
   "address": "Xã Phú Thuận, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.278342,
   "Latitude": 105.4185227
 },
 {
   "STT": 72,
   "Name": "Trạm Y tế Xã Vĩnh Chánh",
   "address": "Xã Vĩnh Chánh, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3023865,
   "Latitude": 105.3716684
 },
 {
   "STT": 73,
   "Name": "Trạm Y tế Xã Định Mỹ",
   "address": "Xã Định Mỹ, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3371899,
   "Latitude": 105.2985487
 },
 {
   "STT": 74,
   "Name": "Trạm Y tế Xã Định Thành",
   "address": "Xã Định Thành, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3057775,
   "Latitude": 105.3014109
 },
 {
   "STT": 75,
   "Name": "Trạm Y tế Xã Mỹ Phú Đông",
   "address": "Xã Mỹ Phú Đông, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.310273,
   "Latitude": 105.2077798
 },
 {
   "STT": 76,
   "Name": "Trạm Y tế Xã Vọng Đông",
   "address": "Xã Vọng Đông, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.2698047,
   "Latitude": 105.1841011
 },
 {
   "STT": 77,
   "Name": "Trạm Y tế Xã Vĩnh Khánh",
   "address": "Xã Vĩnh Khánh, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.2599612,
   "Latitude": 105.348246
 },
 {
   "STT": 78,
   "Name": "Trạm Y tế Xã Thoại Giang",
   "address": "Xã Thoại Giang, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.2655716,
   "Latitude": 105.2311827
 },
 {
   "STT": 79,
   "Name": "Trạm Y tế Xã Bình Thành",
   "address": "Xã Bình Thành, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.2179592,
   "Latitude": 105.2019295
 },
 {
   "STT": 80,
   "Name": "Trạm Y tế Xã Vọng Thê",
   "address": "Xã Vọng Thê, Huyện Thoại Sơn, Tỉnh QUẢNG NINH",
   "Longtitude": 10.275753,
   "Latitude": 105.1317431
 },
 {
   "STT": 81,
   "Name": "Trạm Y tế Thị trấn Phú Mỹ",
   "address": "Thị trấn Phú Mỹ, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5900512,
   "Latitude": 105.3453184
 },
 {
   "STT": 82,
   "Name": "Trạm Y tế Thị trấn Chợ Vàm",
   "address": "Thị trấn Chợ Vàm, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.702865,
   "Latitude": 105.3306814
 },
 {
   "STT": 83,
   "Name": "Trạm Y tế Xã Long Hoà",
   "address": "Xã Long Hoà, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7611065,
   "Latitude": 105.2801455
 },
 {
   "STT": 84,
   "Name": "Trạm Y tế Xã Phú Long",
   "address": "Xã Phú Long, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7242489,
   "Latitude": 105.2311827
 },
 {
   "STT": 85,
   "Name": "Trạm Y tế Xã Phú Lâm",
   "address": "Xã Phú Lâm, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7348122,
   "Latitude": 105.2747469
 },
 {
   "STT": 86,
   "Name": "Trạm Y tế Xã Phú Hiệp",
   "address": "Xã Phú Hiệp, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.707608,
   "Latitude": 105.150979
 },
 {
   "STT": 87,
   "Name": "Trạm Y tế Xã Phú Thạnh",
   "address": "Xã Phú Thạnh, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7000238,
   "Latitude": 105.2779983
 },
 {
   "STT": 88,
   "Name": "Trạm Y tế Xã Hoà Lạc",
   "address": "Xã Hoà Lạc, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6752813,
   "Latitude": 105.2253316
 },
 {
   "STT": 89,
   "Name": "Trạm Y tế Xã Phú Thành",
   "address": "Xã Phú Thành, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6574189,
   "Latitude": 105.2545888
 },
 {
   "STT": 90,
   "Name": "Trạm Y tế Xã Phú An",
   "address": "Xã Phú An, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.692405,
   "Latitude": 105.3396127
 },
 {
   "STT": 91,
   "Name": "Trạm Y tế Xã Phú Xuân",
   "address": "Xã Phú Xuân, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6317038,
   "Latitude": 105.2862284
 },
 {
   "STT": 92,
   "Name": "Trạm Y tế Xã Hiệp Xương",
   "address": "Xã Hiệp Xương, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6073356,
   "Latitude": 105.2721456
 },
 {
   "STT": 93,
   "Name": "Trạm Y tế Xã Phú Bình",
   "address": "Xã Phú Bình, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5933054,
   "Latitude": 105.2493604
 },
 {
   "STT": 94,
   "Name": "Trạm Y tế Xã Phú Thọ",
   "address": "Xã Phú Thọ, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6320592,
   "Latitude": 105.3248269
 },
 {
   "STT": 95,
   "Name": "Trạm Y tế Xã Phú Hưng",
   "address": "Xã Phú Hưng, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5940885,
   "Latitude": 105.3189726
 },
 {
   "STT": 96,
   "Name": "Trạm Y tế Xã Bình Thạnh Đông",
   "address": "Xã Bình Thạnh Đông, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.570495,
   "Latitude": 105.258745
 },
 {
   "STT": 97,
   "Name": "Trạm Y tế Xã Tân Hòa",
   "address": "Xã Tân Hòa, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5690667,
   "Latitude": 105.3277541
 },
 {
   "STT": 98,
   "Name": "Trạm Y tế Xã Tân Trung",
   "address": "Xã Tân Trung, Huyện Phú Tân, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5771188,
   "Latitude": 105.359507
 },
 {
   "STT": 99,
   "Name": "Trạm Y tế Thị trấn Chợ Mới",
   "address": "Thị trấn Chợ Mới, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5474757,
   "Latitude": 105.4053436
 },
 {
   "STT": 100,
   "Name": "Trạm Y tế Thị trấn Mỹ Luông",
   "address": "Thị trấn Mỹ Luông, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4985989,
   "Latitude": 105.4829681
 },
 {
   "STT": 101,
   "Name": "Trạm Y tế Xã Kiến An",
   "address": "Xã Kiến An, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5423179,
   "Latitude": 105.3716684
 },
 {
   "STT": 102,
   "Name": "Trạm Y tế Xã Mỹ Hội Đông",
   "address": "Xã Mỹ Hội Đông, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.515886,
   "Latitude": 105.3541013
 },
 {
   "STT": 103,
   "Name": "Trạm Y tế Xã Long Điền A",
   "address": "Xã Long Điền A, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5325029,
   "Latitude": 105.4595306
 },
 {
   "STT": 104,
   "Name": "Trạm Y tế Xã Tấn Mỹ",
   "address": "Xã Tấn Mỹ, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.508345,
   "Latitude": 105.5064087
 },
 {
   "STT": 105,
   "Name": "Trạm Y tế Xã Long Điền B",
   "address": "Xã Long Điền B, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5112548,
   "Latitude": 105.4478131
 },
 {
   "STT": 106,
   "Name": "Trạm Y tế Xã Kiến Thành",
   "address": "Xã Kiến Thành, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.512995,
   "Latitude": 105.4126652
 },
 {
   "STT": 107,
   "Name": "Trạm Y tế Xã Mỹ Hiệp",
   "address": "Xã Mỹ Hiệp, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5065934,
   "Latitude": 105.5415755
 },
 {
   "STT": 108,
   "Name": "Trạm Y tế Xã Mỹ An",
   "address": "Xã Mỹ An, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4756184,
   "Latitude": 105.5064087
 },
 {
   "STT": 109,
   "Name": "Trạm Y tế Xã Nhơn Mỹ",
   "address": "Xã Nhơn Mỹ, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4756645,
   "Latitude": 105.3950939
 },
 {
   "STT": 110,
   "Name": "Trạm Y tế Xã Long Giang",
   "address": "Xã Long Giang, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4681887,
   "Latitude": 105.4360963
 },
 {
   "STT": 111,
   "Name": "Trạm Y tế Xã Long Kiến",
   "address": "Xã Long Kiến, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4664526,
   "Latitude": 105.471249
 },
 {
   "STT": 112,
   "Name": "Trạm Y tế Xã Bình Phước Xuân",
   "address": "Xã Bình Phước Xuân, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4514826,
   "Latitude": 105.5532993
 },
 {
   "STT": 113,
   "Name": "Trạm Y tế Xã An Thạnh Trung",
   "address": "Xã An Thạnh Trung, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.427419,
   "Latitude": 105.488828
 },
 {
   "STT": 114,
   "Name": "Trạm Y tế Xã Hội An",
   "address": "Xã Hội An, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4330203,
   "Latitude": 105.5508873
 },
 {
   "STT": 115,
   "Name": "Trạm Y tế Xã Hòa Bình",
   "address": "Xã Hòa Bình, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3901353,
   "Latitude": 105.471249
 },
 {
   "STT": 116,
   "Name": "Trạm Y tế Xã Hòa An",
   "address": "Xã Hòa An, Huyện Chợ Mới, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3671932,
   "Latitude": 105.494688
 },
 {
   "STT": 117,
   "Name": "Trạm Y tế Thị trấn An Châu",
   "address": "Thị trấn An Châu, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4377694,
   "Latitude": 105.3892372
 },
 {
   "STT": 118,
   "Name": "Trạm Y tế Xã An Hòa",
   "address": "Xã An Hòa, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4848607,
   "Latitude": 105.3189726
 },
 {
   "STT": 119,
   "Name": "Trạm Y tế Xã Cần Đăng",
   "address": "Xã Cần Đăng, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4595756,
   "Latitude": 105.2779983
 },
 {
   "STT": 120,
   "Name": "Trạm Y tế Xã Vĩnh Hanh",
   "address": "Xã Vĩnh Hanh, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4446287,
   "Latitude": 105.248737
 },
 {
   "STT": 121,
   "Name": "Trạm Y tế Xã Bình Thạnh",
   "address": "Xã Bình Thạnh, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.407341,
   "Latitude": 105.3423909
 },
 {
   "STT": 122,
   "Name": "Trạm Y tế Xã Vĩnh Bình",
   "address": "Xã Vĩnh Bình, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4204614,
   "Latitude": 105.1843801
 },
 {
   "STT": 123,
   "Name": "Trạm Y tế Xã Bình Hòa",
   "address": "Xã Bình Hòa, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.463087,
   "Latitude": 105.3423985
 },
 {
   "STT": 124,
   "Name": "Trạm Y tế Xã Vĩnh An",
   "address": "Xã Vĩnh An, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4227169,
   "Latitude": 105.1375908
 },
 {
   "STT": 125,
   "Name": "Trạm Y tế Xã Hòa Bình Thạnh",
   "address": "Xã Hòa Bình Thạnh, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.407341,
   "Latitude": 105.3423909
 },
 {
   "STT": 126,
   "Name": "Trạm Y tế Xã Vĩnh Lợi",
   "address": "Xã Vĩnh Lợi, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3929765,
   "Latitude": 105.3014109
 },
 {
   "STT": 127,
   "Name": "Trạm Y tế Xã Vĩnh Nhuận",
   "address": "Xã Vĩnh Nhuận, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3952461,
   "Latitude": 105.2545888
 },
 {
   "STT": 128,
   "Name": "Trạm Y tế Xã Tân Phú",
   "address": "Xã Tân Phú, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3836773,
   "Latitude": 105.1551352
 },
 {
   "STT": 129,
   "Name": "Trạm Y tế Xã Vĩnh Thành",
   "address": "Xã Vĩnh Thành, Huyện Châu Thành, Tỉnh QUẢNG NINH",
   "Longtitude": 10.3531435,
   "Latitude": 105.3092927
 },
 {
   "STT": 130,
   "Name": "Trạm Y tế Thị trấn Cái Dầu",
   "address": "Thị trấn Cái Dầu, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5682157,
   "Latitude": 105.2341082
 },
 {
   "STT": 131,
   "Name": "Trạm Y tế Xã Khánh Hòa",
   "address": "Xã Khánh Hòa, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6770184,
   "Latitude": 105.1902297
 },
 {
   "STT": 132,
   "Name": "Trạm Y tế Xã Mỹ Đức",
   "address": "Xã Mỹ Đức, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6711947,
   "Latitude": 105.1651526
 },
 {
   "STT": 133,
   "Name": "Trạm Y tế Xã Mỹ Phú",
   "address": "Xã Mỹ Phú, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.6171281,
   "Latitude": 105.1843801
 },
 {
   "STT": 134,
   "Name": "Trạm Y tế Xã Ô Long Vỹ",
   "address": "Xã Ô Long Vỹ, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5883214,
   "Latitude": 105.1025075
 },
 {
   "STT": 135,
   "Name": "Trạm Y tế Xã Vĩnh Thạnh Trung",
   "address": "Xã Vĩnh Thạnh Trung, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5779931,
   "Latitude": 105.2019295
 },
 {
   "STT": 136,
   "Name": "Trạm Y tế Xã Thạnh Mỹ Tây",
   "address": "Xã Thạnh Mỹ Tây, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5308066,
   "Latitude": 105.1609838
 },
 {
   "STT": 137,
   "Name": "Trạm Y tế Xã Bình Long",
   "address": "Xã Bình Long, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5440566,
   "Latitude": 105.2253316
 },
 {
   "STT": 138,
   "Name": "Trạm Y tế Xã Bình Mỹ",
   "address": "Xã Bình Mỹ, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5250878,
   "Latitude": 105.2779983
 },
 {
   "STT": 139,
   "Name": "Trạm Y tế Xã Bình Thủy",
   "address": "Xã Bình Thủy, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.5285357,
   "Latitude": 105.3189726
 },
 {
   "STT": 140,
   "Name": "Trạm Y tế Xã Đào Hữu Cảnh",
   "address": "Xã Đào Hữu Cảnh, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4893691,
   "Latitude": 105.1142011
 },
 {
   "STT": 141,
   "Name": "Trạm Y tế Xã Bình Phú",
   "address": "Xã Bình Phú, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4917136,
   "Latitude": 105.1785307
 },
 {
   "STT": 142,
   "Name": "Trạm Y tế Xã Bình Chánh",
   "address": "Xã Bình Chánh, Huyện Châu Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.4997871,
   "Latitude": 105.2370339
 },
 {
   "STT": 143,
   "Name": "Trạm Y tế Thị trấn An Phú",
   "address": "Thị trấn An Phú, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7885968,
   "Latitude": 105.0937378
 },
 {
   "STT": 144,
   "Name": "Trạm Y tế Xã Khánh An",
   "address": "Xã Khánh An, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.9471061,
   "Latitude": 105.1054308
 },
 {
   "STT": 145,
   "Name": "Trạm Y tế Thị Trấn Long Bình",
   "address": "Thị Trấn Long Bình, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.9453897,
   "Latitude": 105.0849687
 },
 {
   "STT": 146,
   "Name": "Trạm Y tế Xã Khánh Bình",
   "address": "Xã Khánh Bình, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.9269062,
   "Latitude": 105.0703544
 },
 {
   "STT": 147,
   "Name": "Trạm Y tế Xã Quốc Thái",
   "address": "Xã Quốc Thái, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8910813,
   "Latitude": 105.0732771
 },
 {
   "STT": 148,
   "Name": "Trạm Y tế Xã Nhơn Hội",
   "address": "Xã Nhơn Hội, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8922487,
   "Latitude": 105.0498966
 },
 {
   "STT": 149,
   "Name": "Trạm Y tế Xã Phú Hữu",
   "address": "Xã Phú Hữu, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8835468,
   "Latitude": 105.1142011
 },
 {
   "STT": 150,
   "Name": "Trạm Y tế Xã Phú Hội",
   "address": "Xã Phú Hội, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8142824,
   "Latitude": 105.0732771
 },
 {
   "STT": 151,
   "Name": "Trạm Y tế Xã Phước Hưng",
   "address": "Xã Phước Hưng, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8685489,
   "Latitude": 105.0849687
 },
 {
   "STT": 152,
   "Name": "Trạm Y tế Xã Vĩnh Lộc",
   "address": "Xã Vĩnh Lộc, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.8396644,
   "Latitude": 105.1142011
 },
 {
   "STT": 153,
   "Name": "Trạm Y tế Xã Vĩnh Hậu",
   "address": "Xã Vĩnh Hậu, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7998294,
   "Latitude": 105.1434387
 },
 {
   "STT": 154,
   "Name": "Trạm Y tế Xã Vĩnh Trường",
   "address": "Xã Vĩnh Trường, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7790702,
   "Latitude": 105.1200482
 },
 {
   "STT": 155,
   "Name": "Trạm Y tế Xã Vĩnh Hội Đông",
   "address": "Xã Vĩnh Hội Đông, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7923518,
   "Latitude": 105.0732771
 },
 {
   "STT": 156,
   "Name": "Trạm Y tế Xã Đa Phước",
   "address": "Xã Đa Phước, Huyện An Phú, Tỉnh QUẢNG NINH",
   "Longtitude": 10.7461948,
   "Latitude": 105.1200482
 }
];