var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám đa khoa Phương Mai",
   "address": "Đường Lý Bôn, TP Thái Bình",
   "Longtitude": 20.4536878,
   "Latitude": 106.3331593
 },
 {
   "STT": 2,
   "Name": "Phòng khám Bảo vệ CSSK cán bộ tỉnh",
   "address": "Đường Trần Thánh Tông, TP Thái Bình",
   "Longtitude": 20.4407588,
   "Latitude": 106.3351505
 },
 {
   "STT": 3,
   "Name": "Phòng khám đa khoa cuộc sống",
   "address": "Xã Vũ Hội,  Huyện Vũ Thư, Tỉnh Thái Bình",
   "Longtitude": 20.4123196,
   "Latitude": 106.3552092
 },
 {
   "STT": 4,
   "Name": "Phòng khám đa khoa An Tập",
   "address": "Phường Đề Thám, TP Thái Bình",
   "Longtitude": 20.4464594,
   "Latitude": 106.3382851
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa tư nhân Việt Hàn",
   "address": "Phường Trần Lãm, TP Thái Bình",
   "Longtitude": 20.4415905,
   "Latitude": 106.3561327
 },
 {
   "STT": 6,
   "Name": "Phòng khám đa khoa Phúc Sơn",
   "address": "Xã Thụy Phúc, Huyện Thái Thụy, Thái Bình",
   "Longtitude": 20.5704499,
   "Latitude": 106.4980162
 },
 {
   "STT": 7,
   "Name": "Phòng khám đa khoa Trường Cao đẳng Y tế Thái Bình",
   "address": "Phường Quang Trung, , TP Thái Bình",
   "Longtitude": 20.4373541,
   "Latitude": 106.3319534
 },
 {
   "STT": 8,
   "Name": "Phòng khám đa khoa Quỳnh Côi",
   "address": "Thị Trấn Quỳnh Côi, Huyện Quỳnh Phụ, Tỉnh Thái Bình",
   "Longtitude": 20.6511779,
   "Latitude": 106.3213623
 }
];
