var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm y tế Dự phòng tỉnh",
   "address": "Phố Hoàng Công Chất, TP Thái Bình",
   "Longtitude": 20.4463471,
   "Latitude": 106.3365828
 },
 {
   "STT": 2,
   "Name": "TT Bảo vệ SK bà mẹ và trẻ em",
   "address": "Đường Trần Thánh Tông, TP Thái Bình",
   "Longtitude": 20.4407588,
   "Latitude": 106.3351505
 },
 {
   "STT": 3,
   "Name": "Phòng y tế CN Công ty Cao su sao vàng tại TB",
   "address": "TP Thái Bình",
   "Longtitude": 20.4463471,
   "Latitude": 106.3365828
 },
 {
   "STT": 4,
   "Name": "Trung tâm Chăm sóc sức khoẻ sinh sản",
   "address": "Đường Trần Thánh Tông, TP Thái Bình",
   "Longtitude": 20.4407588,
   "Latitude": 106.3351505
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Công ty TNHH Minh Trí",
   "address": "Đường Bùi Viện, TP Thái Bình",
   "Longtitude": 20.4494086,
   "Latitude": 106.3281529
 },
 {
   "STT": 6,
   "Name": "Bệnh xá Công An tỉnh",
   "address": "Phan Bá Vành, TP Thái  Bình",
   "Longtitude": 20.4334986,
   "Latitude": 106.3272396
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Công ty TNHH may Hưng Nhân",
   "address": "Nguyễn Đức Cảnh, TP Thái Bình",
   "Longtitude": 20.4460341,
   "Latitude": 106.3507247
 },
 {
   "STT": 8,
   "Name": "Y tế cơ quan Bệnh viện đa khoa tỉnh",
   "address": "Đường Lý Bôn, TP Thái Bình",
   "Longtitude": 20.4536878,
   "Latitude": 106.3331593
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Công ty Cổ phần KCN - TBS Sông Trà",
   "address": "Xã Tân Bình, TP Thái Bình",
   "Longtitude": 20.4693246,
   "Latitude": 106.322603
 }
];